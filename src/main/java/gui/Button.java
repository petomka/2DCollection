package gui;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.gui.ComponentListener;
import org.newdawn.slick.gui.GUIContext;
import org.newdawn.slick.gui.MouseOverArea;

/**
 * Button class. Basically a clickable Image.
 * It can also change it's image depending on
 * whether the user's mouse is over the button's
 * area or not.
 */
public class Button extends MouseOverArea {

    private boolean pressed = false;

    private boolean enabled = true;

    public Button(GUIContext container, Image image, int x, int y, ComponentListener listener) {
        super(container, image, x, y, listener);
    }

    public Button(GUIContext container, Image image, int x, int y) {
        super(container, image, x, y);
    }

    public Button(GUIContext container, Image image, int x, int y, int width, int height, ComponentListener listener) {
        super(container, image, x, y, width, height, listener);
    }

    public Button(GUIContext container, Image image, int x, int y, int width, int height) {
        super(container, image, x, y, width, height);
    }

    public Button(GUIContext container, Image image, Shape shape) {
        super(container, image, shape);
    }

    @Override
    public void mousePressed(int button, int mx, int my) {
        if(!enabled) {
            return;
        }
        if (mx >= getX() && mx <= getX() + getWidth() && my >= getY() && my <= getY() + getHeight() && button == Input.MOUSE_LEFT_BUTTON) {
            pressed = true;
        }
    }

    public boolean isPressed() {
        return pressed;
    }

    public void setPressed(boolean pressed) {
        this.pressed = pressed;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public void render(GUIContext container, Graphics g) {
        if(!enabled) {
            return;
        }
        super.render(container, g);
    }
}
