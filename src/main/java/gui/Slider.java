package gui;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.gui.GUIContext;
import org.newdawn.slick.gui.MouseOverArea;

/**
 * A slider for values in a certain range.
 * Dragged with the mouse.
 */
public class Slider extends MouseOverArea {
    private static Image bar;
    private static Image slider;

    static {
        try {
            bar = new Image("img/ui/sliderBar.png", false, Image.FILTER_NEAREST);
            slider = new Image("img/ui/slider.png", false, Image.FILTER_NEAREST);
            slider = slider.getScaledCopy(2f);
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }

    public static Slider createNewSlider(GUIContext gc, int x, int y) throws SlickException {
        return createNewSlider(gc, x, y, 0);
    }

    public static Slider createNewSlider(GUIContext gc, int x, int y, int value) throws SlickException {
        if (value > 100) {
            value = 100;
        }
        if (value < 0) {
            value = 0;
        }
        return new Slider(gc, slider, x, y, value);
    }

    private int x;
    private int y;
    private int value;

    private Slider(GUIContext container, Image img, int x, int y, int value) throws SlickException {
        super(container, img, x + 20 - slider.getWidth() / 2 + value * 2, y);
        this.x = x;
        this.y = y;
        this.value = value;
    }

    @Override
    public void render(GUIContext container, Graphics g) {
        bar.draw(x, y, 2);
        super.render(container, g);
    }

    private boolean dragging = false;

    @Override
    public void setLocation(float x, float y) {
        super.setLocation(x + 20 - slider.getWidth() / 2 + value * 2, y);
        this.x = (int) x;
        this.y = (int) y;
    }

    @Override
    public void mouseDragged(int oldx, int oldy, int newx, int newy) {
        super.mouseDragged(oldx, oldy, newx, newy);
        if (super.isMouseOver() || dragging) {
            dragging = true;
            int v = ((newx - x  - (slider.getWidth()/2)*3) / 2);
            if (v < 0) {
                v = 0;
            }
            if (v > 100) {
                v = 100;
            }
            this.value = v;
            super.setX(x + 20 - slider.getWidth() / 2 + v * 2);
        }
    }

    public int getTrueX() {
        return x;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int v) {
        if (v < 0) {
            v = 0;
        }
        if (v > 100) {
            v = 100;
        }
        this.value = v;
        super.setX(x + 20 - slider.getWidth() / 2 + v * 2);
    }

    @Override
    public void mouseReleased(int button, int mx, int my) {
        super.mouseReleased(button, mx, my);
        dragging = false;
    }

    public boolean isDragging() {
        return dragging;
    }
}
