package gui;

import main.Main;
import main.MusicManager;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.gui.GUIContext;
import org.newdawn.slick.gui.MouseOverArea;
import util.Ticker;

import java.util.concurrent.Callable;

/**
 * This is the MusicBox. Every Feature it has is implemented here.
 * It also offers some methods to set it's sliders value and the corresponding volume,
 * or to change it's position on the screen.
 */
public class MusicBox extends MouseOverArea implements Ticker.Tickable {

    private static Image musicBox;

    static {
        try {
            musicBox = new Image("img/ui/musicBoxExpanded.png");
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }

    private MusicManager manager;

    private Button playpause;
    private Button skip;
    private Button previous;
    private Button loop;

    private Slider volumeSlider;

    private static Image play;
    private static Image playHover;
    private static Image pause;
    private static Image pauseHover;
    private Image skipImg, skipHover, prev, prevHover, loopImg, loopImgHover, loopActive;

    private int titleDisplayOffset = 0;
    private Ticker displayTicker = new Ticker(this);
    private String display = "";

    private Callable<Void> callBack;

    public MusicBox(GUIContext container, int x, int y, MusicManager mgr, Callable<Void> callBack) throws SlickException {
        super(container, musicBox, x, y, musicBox.getWidth(), musicBox.getHeight());
        this.manager = mgr;
        this.callBack = callBack;

        pause = new Image("img/ui/pauseScaled.png");
        pauseHover = new Image("img/ui/pauseScaledHover.png");
        play = new Image("img/ui/playScaled.png");
        playHover = new Image("img/ui/playScaledHover.png");

        if (manager.isPlaying()) {
            playpause = new Button(container, pause, x + super.getWidth() / 2 - pause.getWidth() / 2, y + super.getHeight() - 53);
            playpause.setMouseOverImage(pauseHover);
        } else {
            playpause = new Button(container, play, x + super.getWidth() / 2 - pause.getWidth() / 2, y + super.getHeight() - 53);
            playpause.setMouseOverImage(playHover);
        }


        skipImg = new Image("img/ui/skipScaled.png");
        skipHover = new Image("img/ui/skipScaledHover.png");
        skip = new Button(container, skipImg, x + ((skipImg.getWidth() / 2) * 2 + 1) + super.getWidth() / 2 - skipImg.getWidth() / 2, y + super.getHeight() - 53);
        skip.setMouseOverImage(skipHover);

        prev = new Image("img/ui/previousScaled.png");
        prevHover = new Image("img/ui/previousScaledHover.png");
        previous = new Button(container, prev, x + ((skipImg.getWidth() / 2) * -2 - 1) + super.getWidth() / 2 - prev.getWidth() / 2, y + super.getHeight() - 53);
        previous.setMouseOverImage(prevHover);

        loopImg = new Image("img/ui/loopScaled.png");
        loopActive = new Image("img/ui/loopScaledActive.png");
        loopImgHover = new Image("img/ui/loopScaledHover.png");
        if (manager.isLooping()) {
            loop = new Button(container, loopActive, x + ((loopImg.getWidth() / 2) * 4 + 1) + super.getWidth() / 2 - prev.getWidth() / 2, y + super.getHeight() - 53);
        } else {
            loop = new Button(container, loopImg, x + ((loopImg.getWidth() / 2) * 4 + 1) + super.getWidth() / 2 - prev.getWidth() / 2, y + super.getHeight() - 53);
        }
        loop.setMouseOverImage(loopImgHover);

        volumeSlider = Slider.createNewSlider(container, x + 30, y + super.getHeight() - 54 - 32, (int) (MusicManager.Music_Volume * 100));

        displayTicker.setTickInterval(35);
    }

    public void init() {
        volumeSlider.setValue((int) (MusicManager.Music_Volume * 100));
    }

    @Override
    public void onTick() {
        titleDisplayOffset += 1;
    }

    @Override
    public void render(GUIContext container, Graphics g) {
        super.render(container, g);
        playpause.render(container, g);
        previous.render(container, g);
        skip.render(container, g);
        loop.render(container, g);
        volumeSlider.render(container, g);
        g.drawString(display, this.getX() + 30, this.getY() + 20);
    }

    public void update() {
        displayTicker.tick();
        if (!manager.isPlaying()) {
            playpause.setNormalImage(play);
            playpause.setMouseOverImage(playHover);
        } else {
            playpause.setNormalImage(pause);
            playpause.setMouseOverImage(pauseHover);
        }
        if (manager.isLooping()) {
            loop.setNormalImage(loopActive);
        } else {
            loop.setNormalImage(loopImg);
        }
        if (playpause.isPressed()) {
            playpause.setPressed(false);
            if (manager.isPlaying()) {
                manager.pause();
            } else {
                manager.resume();
            }
        }
        if (skip.isPressed()) {
            skip.setPressed(false);
            manager.nextTrack();
        }
        if (previous.isPressed()) {
            previous.setPressed(false);
            manager.previousTrack();
        }
        if (loop.isPressed()) {
            loop.setPressed(false);
            manager.setLooping(!manager.isLooping());
        }
        float volume = volumeSlider.getValue() / 100f;
        if(volumeSlider.isDragging()) {
            Main.musicManager.setVolume(volume);
        } else {
            setSliderValue(MusicManager.Music_Volume);
        }
        display = "Jetzt spielt: \"" + manager.getTrackTitle() + "\"";
        if (display.length() > 26) {
            if (titleDisplayOffset >= display.length() - 1) {
                titleDisplayOffset = 0;
                displayTicker.setTicks(0);
                displayTicker.setDelay(150);
            }
            display = display.substring(titleDisplayOffset);
            if (display.length() > 26) {
                display = display.substring(0, 26);
            }
        }
    }

    @Override
    public void setLocation(float x, float y) {
        super.setLocation(x, y);
        if (previous == null) {
            return;
        }
        playpause.setX(x + super.getWidth() / 2 - pause.getWidth() / 2);
        previous.setX(x + ((skipImg.getWidth() / 2) * -2 - 1) + super.getWidth() / 2 - prev.getWidth() / 2);
        skip.setX(x + ((skipImg.getWidth() / 2) * 2 + 1) + super.getWidth() / 2 - skipImg.getWidth() / 2);
        loop.setX(x + ((loopImg.getWidth() / 2) * 4 + 1) + super.getWidth() / 2 - prev.getWidth() / 2);
        volumeSlider.setLocation(x + 30, volumeSlider.getY());
    }

    public void setSliderValue(float f) {
        if (f < 0) {
            f = 0;
        } else if (f > 1.0f) {
            f = 1.0f;
        }
        volumeSlider.setValue((int) (f * 100));
    }

    boolean callBackEnabled = true;

    public boolean isCallBackEnabled() {
        return callBackEnabled;
    }

    public void setCallBackEnabled(boolean callBackEnabled) {
        this.callBackEnabled = callBackEnabled;
    }

    @Override
    public void mouseMoved(int oldx, int oldy, int newx, int newy) {
        super.mouseMoved(oldx, oldy, oldx, oldy);
        if (isMouseOver() || !callBackEnabled) {
            return;
        }
        titleDisplayOffset = 0;
        displayTicker.setTicks(0);
        displayTicker.setDelay(150);
        try {
            callBack.call();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
