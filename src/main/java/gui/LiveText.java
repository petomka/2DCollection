package gui;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

/**
 * This is a String that moves vertically on the screen.
 * It updates only when told to.
 * Not more not less
 */
public class LiveText {

    private Color color;

    private String text;

    private int x, y, speed, duration;

    private float fadeOutSpeed;

    private boolean upwards;

    /**
     * Creates a LiveText object. To Display, register it in the Game!
     * @param toDisplay the Text
     * @param initialColor the Color
     * @param x x-Coordinate
     * @param y y-Coordinate
     * @param speed What speed (fastest: 1 for 1 Pixel/duration)
     * @param duration How many update cycles it shall remain
     * @param upwards whether it should fly upwards or downwards
     */
    public LiveText(String toDisplay, Color initialColor, int x, int y, int speed, int duration, boolean upwards){
        text = toDisplay;
        color = initialColor;
        this.x = x;
        this.y = y;
        if(speed <= 0)
            speed = 1;
        this.speed = speed;
        this.duration = duration;
        this.upwards = upwards;
    }

    public void update() {
        if(duration <= 0) {
            return;
        }
        if(duration%speed == 0)
        y += upwards ? -1 : 1;
        --duration;
    }

    public void render(Graphics g) {
        if(duration <= 0) {
            return;
        }
        Color prev = g.getColor();
        g.setColor(color);
        g.drawString(text, x, y);
        g.setColor(prev);
    }

    public Color getColor() {
        return color;
    }

    public String getText() {
        return text;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getSpeed() {
        return speed;
    }

    public int getDuration() {
        return duration;
    }

    public float getFadeOutSpeed() {
        return fadeOutSpeed;
    }
}
