package gui;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

/**
 * Unused until now
 */
public class StatBar {

    Image background;

    Image gradient;

    int fillgrade = 0;

    private int x, y, curr, max;

    boolean drawString = true;

    /**
     * Creates a new Bar to display a statistic with a percentage value. Respectively, the stat to display
     * should have a maximum and a minimum.
     * @param pathToBackground 320x64 Image
     * @param pathToGradient 3x58 Image
     * @throws SlickException
     */
    public StatBar(String pathToBackground, String pathToGradient, int x, int y) throws SlickException {
        background = new Image(pathToBackground);
        gradient = new Image(pathToGradient);
        this.x = x;
        this.y = y;
    }

    public StatBar(String bg, String ptg) throws SlickException {
        this(bg, ptg, 0,0);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setFillGrade(int curr, int max) {
        this.curr = curr;
        this.max = max;
        if(max%100 != 0) {
            curr = curr * 100/max;
            max = max * 100/max;
        }
        int p = (int)(max*(curr/100f));
        setFillGrade(p);

    }

    public void setFillGrade(int p) {
        if(p > 100)
            p = 100;
        if(p < 0)
            p = 0;
        fillgrade = p;
    }

    public void render(Graphics g) {
        g.drawImage(background, x, y);
        for(int i = 0; i < fillgrade; i++) {
            g.drawImage(gradient, x+3+(i*3), y+3);
        }
        if(drawString) {
            g.drawString(curr + "/" + max + "(" + fillgrade + "%)", x, y+66);
        }
    }

    public void setDrawString(boolean drawString) {
        this.drawString = drawString;
    }

    public int getWidth() {
        return background.getWidth();
    }

    public int getHeight() {
        return background.getHeight();
    }

}
