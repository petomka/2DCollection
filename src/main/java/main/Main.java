package main;

import lombok.Getter;
import minigames.asteroids.AsteroidsMain;
import minigames.brickbreaker.BrickbreakerMain;
import minigames.match3.Match3Main;
import minigames.minesweeper.MinesweeperMain;
import minigames.snake2d.Snake2DMain;
import minigames.tetris.TetrisMain;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import states.MainMenu;

import java.awt.*;

/**
 * Created by Benedikt on 22.05.2017.
 */
public class Main extends StateBasedGame {
    /*VM Options: -Djava.library.path=libs/
    lib Ordner einfügen!*/
    public static final String name = "Raid the Arcade";

    private static AppGameContainer appgc;
    private GameContainer gc;

    @Getter
    private static Main instance;

    public static MusicManager musicManager;

    public static final String LINE_SEPERATOR = System.getProperty("line.separator");

    public Main(String name) throws SlickException {
        super(name);
        instance = this;
        this.addState(new MainMenu());
        musicManager = new MusicManager();
        musicManager.play();
    }

    public static int getWindowHeight() {
    	return appgc.getHeight();
	}

	public static int getWindowWidth() {
    	return appgc.getWidth();
	}

    public static void main(String[] args) {
        try {
            appgc = new AppGameContainer(new Main(name));
            appgc.setDisplayMode(1300, 864, false);
            appgc.setTargetFrameRate(60);
            appgc.setShowFPS(false);
            appgc.setMinimumLogicUpdateInterval(20);
            appgc.setMaximumLogicUpdateInterval(20);
            appgc.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Initializes all available states and loads them so they can be accessed.
     * It also enters state 0, which is the main menu.
     * @param gameContainer The calling game
     * @throws SlickException SlickException
     */
    @Override
    public void initStatesList(GameContainer gameContainer) throws SlickException {
        this.gc = gameContainer;
        this.addState(new Snake2DMain());
        this.addState(new TetrisMain());
        this.addState(new Match3Main());
        this.addState(new MinesweeperMain());
        this.addState(new AsteroidsMain());
        this.addState(new BrickbreakerMain());
        this.getState(0).init(gc, this);
        this.enterState(0);
    }

    /**
     * Enters a state with the given ID
     * @param id the ID of the state to enter
     * @throws SlickException SlickException
     */
    public void updateState(int id) throws SlickException {
        this.getState(id).init(gc, this);
        this.enterState(id);
    }

    /**
     * Same as ((Main)gameContainer).updateState(int);
     * @param ID
     */
    public static void useState(int ID) throws SlickException {
        instance.updateState(ID);
    }

}
