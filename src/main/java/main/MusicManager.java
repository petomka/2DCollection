package main;

import javafx.util.Pair;
import org.newdawn.slick.Music;
import org.newdawn.slick.MusicListener;
import org.newdawn.slick.SlickException;

import java.util.*;

/**
 * Created by Benedikt on 11.06.2017.
 */
public class MusicManager implements MusicListener {

    public static float Music_Volume = 0.05f;

    private Music currentMusic;

    private Pair RASPUTIN_MUSIC;

    int currentTrack = 0;

    private ArrayList<Pair<Music, String>> musics = new ArrayList<>();
    private boolean looping = false;

    public MusicManager() throws SlickException {
        //currentMusic = new Music("sfx/backgroundMusic/xeon6.ogg");

        musics.add(new Pair<>(new Music("sfx/backgroundMusic/xeon6.ogg"), "Xeon 6"));
        musics.add(new Pair<>(new Music("sfx/backgroundMusic/skulls_adventure.ogg"), "Skulls Adventure"));
        musics.add(new Pair<>(new Music("sfx/backgroundMusic/Arcade fast flow.ogg"), "Arcade fast Flow"));

        RASPUTIN_MUSIC = new Pair<>(new Music("sfx/backgroundMusic/RASPUTIN.ogg"), "RASPUTIN - FUNK OVERLOAD");

        currentTrack = (int)(Math.random()*musics.size());
        currentMusic = musics.get(currentTrack).getKey();
        addListener(this);
    }

    boolean rasputin = false;

    public void RASPUTIN () throws SlickException {
        if(rasputin) {
            return;
        }
        musics.add(RASPUTIN_MUSIC);
        if(Music_Volume < 0.5f) {
            Music_Volume = 0.5f + Music_Volume;
        }
        stop();
        currentTrack = musics.indexOf(RASPUTIN_MUSIC);
        currentMusic = musics.get(currentTrack).getKey();
        currentMusic.addListener(this);
        play();
        rasputin = true;
    }

    public void addListener(MusicListener listener) {
        currentMusic.addListener(listener);
    }

    @Override
    public void musicEnded(Music music) {
        if(looping) {
            play();
        } else {
            nextTrack();
        }
    }

    @Override
    public void musicSwapped(Music music, Music music1) {

    }

    public void play() {
        this.play(Music_Volume);
    }

    public void play(float volume) {
        Music_Volume = volume;
        currentMusic.play(1f, Music_Volume);
    }

    public void setVolume(float f) {
        Music_Volume = f;
        setVolume();
    }

    public void setVolume() {
        currentMusic.setVolume(Music_Volume);
    }

    public void nextTrack() {
        currentTrack += 1;
        if(currentTrack >= musics.size()) {
            currentTrack = 0;
        }
        currentMusic = musics.get(currentTrack).getKey();
        addListener(this);
        stop();
        play();
    }

    public void previousTrack() {
        currentTrack -= 1;
        if(currentTrack < 0) {
            currentTrack = musics.size()-1;
        }
        currentMusic = musics.get(currentTrack).getKey();
        addListener(this);
        stop();
        play();
    }

    private void stop() {
        currentMusic.stop();
    }

    public boolean isLooping() {
        return looping;
    }

    public void setLooping(boolean looping) {
        this.looping = looping;
    }

    public String getTrackTitle() {
        return musics.get(currentTrack).getValue();
    }

    public void pause() {
        currentMusic.pause();
    }

    public void resume() {
        currentMusic.resume();
    }

    public boolean isPlaying() {
        return currentMusic.playing();
    }
}
