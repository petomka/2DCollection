package states;

import main.Main;
import org.lwjgl.Sys;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

/**
 * Created by Benedikt on 18.06.2017.
 */
public abstract class SuperState extends BasicGameState {

    @Override
    public final void init(GameContainer gameContainer, StateBasedGame stateBasedGame) throws SlickException {
        in = gameContainer.getInput();
        initS(gameContainer, stateBasedGame);
    }

    @Override
    public final void render(GameContainer gameContainer, StateBasedGame stateBasedGame, Graphics graphics) throws SlickException {
        renderS(gameContainer, stateBasedGame, graphics);
    }

    String rasputin = "";

    Input in;

    @Override
    public void keyPressed(int key, char c) {
        super.keyPressed(key, c);
        c = Character.toLowerCase(c);
        if (c == 'r' || c  == 'a' || c == 's' || c == 'p' || c == 'u' || c == 't' || c == 'i' || c == 'n') {
            if(c == 'r') {
                rasputin = "R";
            } else if(c == 'a' && rasputin.equals("R")) {
                rasputin = "RA";
            } else if(c == 's' && rasputin.equals("RA")) {
                rasputin = "RAS";
            } else if(c == 'p' && rasputin.equals("RAS")) {
                rasputin = "RASP";
            } else if(c == 'u' && rasputin.equals("RASP")) {
                rasputin = "RASPU";
            } else if(c == 't' && rasputin.equals("RASPU")) {
                rasputin = "RASPUT";
            } else if(c == 'i' && rasputin.equals("RASPUT")) {
                rasputin = "RASPUTI";
            } else if(c == 'n' && rasputin.equals("RASPUTI")) {
                rasputin = "RASPUTIN";
                try {
                    Main.musicManager.RASPUTIN();
                } catch (Exception e) {
                    Sys.alert(e.getCause().toString(), e.getMessage());
                }
            } else {
                rasputin = "";
            }
            return;
        }
        rasputin = "";
    }

    @Override
    public final void update(GameContainer gameContainer, StateBasedGame stateBasedGame, int i) throws SlickException {

        updateS(gameContainer, stateBasedGame, i);
    }


    public void initS(GameContainer gameContainer, StateBasedGame stateBasedGame) throws SlickException {

    }


    public void renderS(GameContainer gameContainer, StateBasedGame stateBasedGame, Graphics graphics) throws SlickException {

    }


    public void updateS(GameContainer gameContainer, StateBasedGame stateBasedGame, int i) throws SlickException {

    }
}
