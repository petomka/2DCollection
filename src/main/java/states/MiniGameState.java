package states;

import gui.Button;
import gui.MusicBox;
import lombok.Getter;
import main.Main;
import org.newdawn.slick.*;
import org.newdawn.slick.state.StateBasedGame;

import java.util.Random;

/**
 * Created by Benedikt on 22.05.2017.
 */
@Getter
public abstract class MiniGameState extends SuperState implements MusicListener {

    private final int ID;

    private boolean showHelpScreen = false;
    private boolean showSettingsScreen = false;
    private boolean bool_musicBoxExpanded = false;
    private boolean bool_musicBoxAnimated = false;

    protected Button escape, help, closeHelp, settings, closeSettings, musicBoxClosed;
    protected Image escapeImage, escapeImageHover, helpImage, helpImageHover, helpWindow, wasd, settingsImage, settingsImageHover, settingsScreen;

    private int musicBoxSlideInt = 0;

    protected Input in;

    protected Random generator = new Random();

    public MiniGameState(int id) {
        this.ID = id;
    }

    protected boolean paused;

    protected MusicBox musicBox;

    @Override
    public final int getID() {
        return ID;
    }

    @Override
    public final void initS(GameContainer gameContainer, StateBasedGame stateBasedGame) throws SlickException {
        in = gameContainer.getInput();

        escapeImage = new Image("img/escapeButtonScaled.png");
        escapeImageHover = new Image("img/escapeButtonScaledHover.png");
        helpImage = new Image("img/questionButtonScaled.png");
        helpImageHover = new Image("img/questionButtonScaledHover.png");

        escape = new Button(gameContainer, escapeImage, 10, 10);
        escape.setMouseOverImage(escapeImageHover);

        help = new Button(gameContainer, helpImage, 70, 10);
        help.setMouseOverImage(helpImageHover);

        helpWindow = new Image("img/basicHelpWindow.png");
        closeHelp = new Button(gameContainer, escapeImage, 110, 110);
        closeHelp.setEnabled(false);
        closeHelp.setMouseOverImage(escapeImageHover);

        settingsScreen = new Image("img/settingsScreen.png");

        settingsImage = new Image("img/exclamationButtonScaled.png");
        settingsImageHover = new Image("img/exclamationButtonScaledHover.png");
        settings = new Button(gameContainer, settingsImage, 130, 10);
        settings.setMouseOverImage(settingsImageHover);

        closeSettings = new Button(gameContainer, escapeImage, 10, 10);
        closeSettings.setMouseOverImage(escapeImageHover);

        Image mbc =  new Image("img/ui/musicBoxClosed.png");
        musicBoxClosed = new Button(gameContainer, mbc, 1300-mbc.getWidth(), 864-160);

        wasd = new Image("img/wasdScaled.png");

        if(musicBox == null) {
            musicBox = new MusicBox(gameContainer, 1300, musicBoxClosed.getY() - 85 + musicBoxClosed.getHeight()/2, Main.musicManager, () -> {
                bool_musicBoxExpanded = false;
                return null;
            });
        }

        musicBox.init();

        initG(gameContainer, stateBasedGame);
    }

    @Override
    public final void renderS(GameContainer gameContainer, StateBasedGame stateBasedGame, Graphics graphics) throws SlickException {
        if(showSettingsScreen) {
            settingsScreen.draw(0,0);
            closeSettings.render(gameContainer, graphics);
            return;
        }
        renderG(gameContainer, stateBasedGame, graphics);
        escape.render(gameContainer, graphics);
        help.render(gameContainer, graphics);
        settings.render(gameContainer, graphics);
        if(showHelpScreen) {
            renderHelpScreen(graphics);
            closeHelp.render(gameContainer, graphics);
        }
        if(!bool_musicBoxExpanded && !bool_musicBoxAnimated) {
            musicBoxClosed.render(gameContainer, graphics);
        } else {
            musicBox.render(gameContainer, graphics);
        }
    }

    @Override
    public final void updateS(GameContainer gameContainer, StateBasedGame stateBasedGame, int i) throws SlickException {
        if(escape.isPressed()) {
            escape.setPressed(false);
            resetMinigame();
            ((Main)stateBasedGame).updateState(0);
            return;
        }
        if(help.isPressed()) {
            help.setPressed(false);
            showHelpScreen = true;
            closeHelp.setEnabled(true);
            escape.setEnabled(false);
            help.setEnabled(false);
            settings.setEnabled(false);
            paused = true;
        }
        if(closeHelp.isPressed()) {
            closeHelp.setPressed(false);
            closeHelp.setEnabled(false);
            showHelpScreen = false;
            escape.setEnabled(true);
            help.setEnabled(true);
            settings.setEnabled(true);
        }
        if(settings.isPressed()) {
            settings.setPressed(false);
            showSettingsScreen = true;
            closeSettings.setEnabled(true);
            escape.setEnabled(false);
            help.setEnabled(false);
            settings.setEnabled(false);
            paused = true;
        }
        if(closeSettings.isPressed()) {
            closeSettings.setPressed(false);
            closeSettings.setEnabled(false);
            showSettingsScreen = false;
            escape.setEnabled(true);
            help.setEnabled(true);
            settings.setEnabled(true);
        }
        if(bool_musicBoxExpanded) {
            musicBox.update();
            if(musicBox.getX() + musicBox.getWidth() > 1300) {
                musicBox.setLocation(musicBox.getX() - 13f, musicBox.getY());
            } else {
                musicBox.setLocation(1300-musicBox.getWidth(), musicBox.getY());
            }
        } else {
            if(musicBox.getX() < 1300) {
                musicBox.setLocation(musicBox.getX() + 13f, musicBox.getY());
                bool_musicBoxAnimated = true;
            } else {
                musicBox.setLocation(1300, musicBox.getY());
                bool_musicBoxAnimated = false;
            }
        }
        if(musicBoxClosed.isPressed()) {
            musicBoxClosed.setPressed(false);
            bool_musicBoxExpanded = true;
            musicBox.setLocation(musicBox.getX()-musicBoxClosed.getWidth(), musicBox.getY());
        }
        updateG(gameContainer, stateBasedGame, i);
    }

    public final void renderHelpScreen(Graphics g) {
        helpWindow.draw(0,0);
        renderHelpScreenG(g);
    }

    @Override
    public void musicEnded(Music music) {

    }

    @Override
    public void musicSwapped(Music music, Music music1) {

    }

    public final void setPaused(boolean paused) {
        this.paused = paused;
    }

    public abstract void initG(GameContainer gc, StateBasedGame game) throws SlickException;

    public abstract void updateG(GameContainer gc, StateBasedGame game, int i) throws SlickException;

    public abstract void renderG(GameContainer gc, StateBasedGame game, Graphics g) throws SlickException;

    public abstract void resetMinigame();

    public abstract void renderHelpScreenG(Graphics g);
}
