package states;

import gui.Button;
import gui.MusicBox;
import gui.Slider;
import main.Main;
import main.MusicManager;
import minigames.asteroids.AsteroidsMain;
import minigames.brickbreaker.BrickbreakerMain;
import minigames.match3.Match3Main;
import minigames.snake2d.Snake2DMain;
import minigames.tetris.TetrisMain;
import org.newdawn.slick.*;
import org.newdawn.slick.state.StateBasedGame;

/**
 * Created by Benedikt on 22.05.2017.
 */
public class MainMenu extends SuperState {

    public static final int ID = 0;

    Image background;

//    Button tennisForTwo;
//    Image tennisForTwoImage;
//    Image tennisForTwoImageHover;

    //Zuma, Gemsweeper-Like, Sudoku, Alien Invasion, Azteca-Like, Frogger (Crossy Road)

    private Button snake2D, tetris, match3, asteroids, brickbreaker;
    private Image snake2DImage, tetrisImage, match3Image, asteroidsImage, brickbreakerImage;
    private Image snake2DImageHover, tetrisImageHover, match3ImageHover, asteroidsImageHover, brickBreakerImageHover;

    private static MusicBox musicBox;

    private Slider musicVolumeSlider;

    @Override
    public int getID() {
        return ID;
    }

    @Override
    public void initS(GameContainer gameContainer, StateBasedGame stateBasedGame) throws SlickException {
        /*
         * 20 Ticks pro Sekunde
         */
        gameContainer.setMinimumLogicUpdateInterval(20);
        gameContainer.setMaximumLogicUpdateInterval(20);

        background = new Image("img/background.jpg");

        snake2DImage = new Image("img/snake2DPreview.png");
        snake2DImageHover = new Image("img/snake2DPreviewHover.png");
        snake2D = new Button(gameContainer, snake2DImage, 40, 40);
        snake2D.setMouseOverImage(snake2DImageHover);

        tetrisImage = new Image("img/tetrisPreview.png");
        tetrisImageHover = new Image("img/tetrisPreviewHover.png");
        tetris = new Button(gameContainer, tetrisImage, 240 + 40 + 40, 40);
        tetris.setMouseOverImage(tetrisImageHover);

        match3Image = new Image("img/match3Preview.png");
        match3ImageHover = new Image("img/match3PreviewHover.png");
        match3 = new Button(gameContainer, match3Image, 240 * 2 + 40 * 2 + 40, 40);
        match3.setMouseOverImage(match3ImageHover);

        asteroidsImage = new Image("img/asteroidsPreview.png");
        asteroidsImageHover = new Image("img/asteroidsPreviewHover.png");
        asteroids = new Button(gameContainer, asteroidsImage, 240 * 3 + 40 * 4, 40);
        asteroids.setMouseOverImage(asteroidsImageHover);

        brickbreakerImage = new Image("img/brickbreakerPreview.png");
        brickBreakerImageHover = new Image("img/brickbreakerPreviewHover.png");
        brickbreaker = new Button(gameContainer, brickbreakerImage, 40, 40 + 135 + 40);
        brickbreaker.setMouseOverImage(brickBreakerImageHover);

        if(musicVolumeSlider == null) {
            musicVolumeSlider = Slider.createNewSlider(gameContainer, 1300 - 250, 864 - 40, (int) (MusicManager.Music_Volume * 100));
        } else {
            musicBox.setSliderValue(MusicManager.Music_Volume);
        }

        if (musicBox == null) {
            musicBox = new MusicBox(gameContainer, 1000, 864 - 200, Main.musicManager, () -> null);
        }

    }

    @Override
    public void renderS(GameContainer gameContainer, StateBasedGame stateBasedGame, Graphics graphics) throws SlickException {
        background.draw();
        snake2D.render(gameContainer, graphics);
        tetris.render(gameContainer, graphics);
        match3.render(gameContainer, graphics);
        asteroids.render(gameContainer, graphics);
        brickbreaker.render(gameContainer, graphics);
        //musicVolumeSlider.render(gameContainer, graphics);
        musicBox.render(gameContainer, graphics);
        signature(graphics);
    }

    @Override
    public void updateS(GameContainer gameContainer, StateBasedGame stateBasedGame, int i) throws SlickException {
        if(stateBasedGame.getCurrentState().getID() == ID) {
            musicBox.update();
            musicBox.setCallBackEnabled(false);
        } else {
            musicBox.setCallBackEnabled(true);
        }
        if (snake2D.isPressed()) {
            snake2D.setPressed(false);
            Main.useState(Snake2DMain.ID);
            return;
        }
        if (tetris.isPressed()) {
            tetris.setPressed(false);
            Main.useState(TetrisMain.ID);
            return;
        }
        if (match3.isPressed()) {
            match3.setPressed(false);
            Main.useState(Match3Main.ID);
            return;
        }
        if (asteroids.isPressed()) {
            asteroids.setPressed(false);
            Main.useState(AsteroidsMain.ID);
            return;
        }
        if (brickbreaker.isPressed()) {
            brickbreaker.setPressed(false);
            Main.useState(BrickbreakerMain.ID);
            return;
        }
//        float volume = musicVolumeSlider.getValue() / 100f;
//        Main.musicManager.setVolume(volume);
    }

    private void signature(Graphics g) {
        Color prev = g.getColor();
        g.drawString("TeamSpeak3: reallifesucks.de | by Bene", 10, 814);
        g.setColor(Color.white);
        g.setColor(prev);
    }
}
