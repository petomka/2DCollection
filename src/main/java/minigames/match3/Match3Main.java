package minigames.match3;

import org.newdawn.slick.*;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.state.StateBasedGame;
import states.MiniGameState;
import util.Ticker;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Benedikt on 25.05.2017.
 * Main Class for the Match3 Game
 */
public class Match3Main extends MiniGameState implements Ticker.Tickable {

    public static final int ID = 4;

    private Image background;

    private Image gemRed, gemLightBlue, gemGreen, gemDarkGreen, gemPurple, gemPink, gemBlue, gemYellow;

    private Image gemRedHighlight, gemLightBlueHighlight, gemGreenHighlight, gemDarkGreenHighlight, gemPurpleHighlight, gemPinkHighlight, gemBlueHighlight, gemYellowHighlight;

    private Image[] imageID;

    private Image[] imageHighlightID;

    private int[][] field = new int[20][20];

    private boolean begun = false, checkFieldForZero = true;

    private HashMap<FallingTile, Ticker> fallingTiles = new HashMap<>();

    private ArrayList<Point> highlightedTiles = new ArrayList<>();

    private ArrayList<DroppedTile> droppedTiles = new ArrayList<>();

    private ArrayList<ArrayList<Point>> possibleMoves = new ArrayList<>();

    private ArrayList<HintTile> hintTiles = new ArrayList<>();

    private boolean showHint = false;

    private Ticker hintTicker = new Ticker(this);

    private boolean[] droppingColumns = new boolean[field[0].length];

    private int xOffset = 250, yOffset = 35;

    private int differentGemCount = 4;

    private int score = 0;

    private int level = 0;

    private int scoreGoal;

    private int currentTime = 0;

    private boolean gameOver = false;

    public Match3Main() {
        super(ID);
    }

    @Override
    public void onTick() {
        if (showHint) {
            return;
        }
        randomHint();
        showHint = true;
    }

    private void randomHint() {
        hintTiles.clear();
        if (possibleMoves.isEmpty()) {
            return;
        }
        ArrayList<Point> toHint = possibleMoves.get(generator.nextInt(possibleMoves.size()));
        for (Point p : toHint) {
            hintTiles.add(new HintTile(field[p.y][p.x], p.x, p.y));
        }
    }

    @Override
    public void initG(GameContainer gc, StateBasedGame game) throws SlickException {
        background = new Image("img/match3/mineBackgroundScaledUI.png");

        gemRed = new Image("img/match3/gemRed.png");
        gemLightBlue = new Image("img/match3/gemLightBlue.png");
        gemGreen = new Image("img/match3/gemGreen.png");
        gemDarkGreen = new Image("img/match3/gemDarkGreen.png");
        gemPurple = new Image("img/match3/gemPurple.png");
        gemPink = new Image("img/match3/gemPink.png");
        gemBlue = new Image("img/match3/gemBlue.png");
        gemYellow = new Image("img/match3/gemYellow.png");

        gemRedHighlight = new Image("img/match3/gemRedHighlight.png");
        gemLightBlueHighlight = new Image("img/match3/gemLightBlueHighlight.png");
        gemGreenHighlight = new Image("img/match3/gemGreenHighlight.png");
        gemDarkGreenHighlight = new Image("img/match3/gemDarkGreenHighlight.png");
        gemPurpleHighlight = new Image("img/match3/gemPurpleHighlight.png");
        gemPinkHighlight = new Image("img/match3/gemPinkHighlight.png");
        gemBlueHighlight = new Image("img/match3/gemBlueHighlight.png");
        gemYellowHighlight = new Image("img/match3/gemYellowHighlight.png");

        imageID = new Image[]{gemRed, gemYellow, gemGreen, gemDarkGreen, gemBlue, gemLightBlue, gemPink, gemPurple};

        imageHighlightID = new Image[]{gemRedHighlight, gemYellowHighlight, gemGreenHighlight, gemDarkGreenHighlight, gemBlueHighlight, gemLightBlueHighlight, gemPinkHighlight, gemPurpleHighlight};

        hintTicker.setTickInterval(500);
    }

    @Override
    public void updateG(GameContainer gc, StateBasedGame game, int i) throws SlickException {
        if(gameOver) {
            begun = false;
            if(in.isKeyPressed(Input.KEY_SPACE)) {
                resetMinigame();
                for (int a = 0; a < field.length; a++) {
                    fillColumn(a);
                }
                gameOver = false;
                begun = true;
                return;
            }
        }
        if (begun && in.isKeyPressed(Input.KEY_SPACE) && !gameOver) {
            paused = !paused;
        }
        if (!begun && !gameOver) {
            if (in.isKeyPressed(Input.KEY_SPACE) || gameOver) {
                begun = true;
                gameOver = false;
                for (int a = 0; a < field.length; a++) {
                    fillColumn(a);
                }
            }
        }

        if(paused || !begun || gameOver) {
            return;
        }
        checkScoreGoal();
        currentTime -= i;
        if(currentTime <= 0) {
            gameOver = true;
        }
        ArrayList<FallingTile> rem = new ArrayList<>();
        for (FallingTile ft : fallingTiles.keySet()) {
            fallingTiles.get(ft).tick();
            if (ft.isDone()) {
                rem.add(ft);
            }
        }
        for (FallingTile ft : rem) {
            fallingTiles.remove(ft);
            field[(ft.getCoords().y - yOffset) / 40][(ft.getCoords().x - xOffset) / 40] = ft.getGemID() + 1;
            updateHighlights((in.getMouseX() - xOffset) / 40, (in.getMouseY() - yOffset) / 40);
            droppingColumns[(ft.getCoords().x - xOffset) / 40] = false;
        }
        ArrayList<DroppedTile> remDT = new ArrayList<>();
        for (DroppedTile dt : droppedTiles) {
            dt.ticker.tick();
            if (dt.isDone()) {
                remDT.add(dt);
            }
        }
        droppedTiles.removeAll(remDT);
        if (fallingTiles.isEmpty() && checkFieldForZero && begun) {
            checkFieldForZero = false;
            for (int a = 0; a < field.length; a++) {
                for (int b = 0; b < field.length; b++) {
                    if (field[a][b] == 0) {
                        dropColumn(b);
                        checkFieldForZero = true;
                    }
                }
            }
        }
        for (HintTile ht : hintTiles) {
            ht.ticker.tick();
        }
        hintTicker.tick();
    }

    @Override
    public void renderG(GameContainer gc, StateBasedGame game, Graphics g) throws SlickException {
        background.draw(0, 0);
        {
            int cTime = currentTime;
            int mins = 0;
            int secs = 0;
            while(cTime > 0) {
                if(cTime >= 59000) {
                    ++mins;
                    cTime -= 60000;
                    continue;
                }
                ++secs;
                cTime -= 1000;
            }
            g.drawString("Farben: " + differentGemCount, 1100, 150);
            g.drawString("Ziel  : " + scoreGoal, 1100, 200);
            g.drawString("Punkte: " + score, 1100, 250);
            g.drawString("Zeit  : " + (mins <= 9 ? "0" + mins : mins) + ":" + (secs <= 9 ? "0" + secs : secs), 1100, 600);
        }
        if (!begun && !gameOver) {
            g.drawString("Drücke LEER um das Spiel zu starten!", 460, 440);
        }
        if (paused && begun && !gameOver) {
            g.drawString("Pausiert!", 600, 440);
            return;
        }
        for (FallingTile ft : fallingTiles.keySet()) {
            if (ft.gemID < 0) {
                continue;
            }
            imageID[ft.getGemID()].setAlpha(1.0f - ft.getOpacity());
            imageID[ft.getGemID()].draw(ft.getCoords().x, ft.getCoords().y);
            imageID[ft.getGemID()].setAlpha(1f);
        }
        for (DroppedTile dt : droppedTiles) {
            if(dt.gemID < 0) {
                return;
            }
            imageID[dt.gemID].setAlpha(dt.getOpacity());
            imageID[dt.gemID].rotate((dt.clockwise ? 1 : -1) * dt.getAngle());
            imageID[dt.gemID].draw(dt.position.x, dt.position.y);
            imageID[dt.gemID].setAlpha(1f);
            imageID[dt.gemID].rotate((dt.clockwise ? -1 : 1) * dt.getAngle());
        }
        for (int a = 0; a < field.length; a++) {
            for (int b = 0; b < field[0].length; b++) {
                if (field[a][b] <= 0) {
                    continue;
                }
                if (field[a][b] > imageID.length) {
                    continue;
                }
                imageID[field[a][b] - 1].draw(xOffset + b * 40, yOffset + a * 40);
            }
        }
        for (Point p : highlightedTiles) {
            if (field[p.y][p.x] > 0 && field[p.y][p.x] <= imageHighlightID.length) {
                imageHighlightID[field[p.y][p.x] - 1].draw(xOffset + p.x * 40, yOffset + p.y * 40);
            }
        }
        for (HintTile ht : hintTiles) {
            if (ht.gemID < 1) {
                break;
            }
            imageHighlightID[ht.gemID - 1].setAlpha(ht.getOpacity());
            imageHighlightID[ht.gemID - 1].draw(ht.pos.x, ht.pos.y);
            imageHighlightID[ht.gemID - 1].setAlpha(1f);
        }
        if(gameOver) {
            Color prev = g.getColor();
            g.setColor(Color.red);
            g.drawString("Game Over! Drücke LEER um neuzustarten!", 430, 440);
            g.setColor(prev);
        }
    }

    @Override
    public void resetMinigame() {
        begun = false;
        paused = false;
        gameOver = false;
        field = new int[20][20];
        checkFieldForZero = true;
        droppingColumns = new boolean[field[0].length];
        fallingTiles.clear();
        highlightedTiles.clear();
        hintTiles.clear();
        possibleMoves.clear();
        hintTicker.setTicks(0);
        differentGemCount = 4;
        currentTime = 0;
        level = 0;
        score = 0;
        scoreGoal = 0;
    }

    private void checkScoreGoal() {
        if(score < scoreGoal) {
            return;
        }
        scoreGoal += (scoreGoal/2 + level*650)*1.2;
        level += 1;
        if(level%3 == 0) {
            if(differentGemCount < imageID.length) {
                differentGemCount += 1;
            }
        }
        currentTime += level*differentGemCount*3600;
    }

    @Override
    public void renderHelpScreenG(Graphics g) {
        g.setColor(Color.white);
        g.drawString("Das Ziel ist es, in begrenzter Zeit möglichst viele Punkte zu sammeln. Sammle Punkte, indem du eine Gruppe von drei \n" +
                "oder mehr gleichen Edelsteinen mit der linken Maustaste anklickst. Jedes Mal, wenn du ein bestimmtes Punkteziel\n" +
                "erreichst, bekommst du zusätzliche Zeit. Unter anderem erhöht sich auch die Anzahl der unterschiedlichen Edelsteine.\n\n" +
                "Es gibt acht verschieden Edelsteine, welche in dieser Reihenfolge erscheinen werden:", 110, 170);
        for (int i = 0; i < imageID.length; i++) {
            imageID[i].draw(110 + i * 40, 280);
        }
        g.setColor(Color.white);
        g.drawString("Wenn du einige Zeit keine 3er-Kombination gefunden hast, bekommst du eine mögliche Gruppe angezeigt.\n" +
                "Sollten keine Gruppen mehr verfügbar sein, wird das Feld komplett neu ausgewürfelt.", 110, 350);
        g.setColor(Color.yellow);
        g.drawString("Du kannst das Spiel jederzeit mit der Leertaste pausieren!", 370, 500);
        g.setColor(Color.white);
    }

    private void fillColumn(int c) {
        int[] gems = new int[field[0].length];
        for (int i = 0; i < gems.length; i++) {
            gems[i] = generator.nextInt(differentGemCount);
        }
        for (int i = 0; i < gems.length; i++) {
            FallingTile ft = new FallingTile(gems[i], xOffset + c * 40, yOffset + 17 * 40 - i * 40, yOffset + (gems.length - 1 - i) * 40, true);
            Ticker t = new Ticker(ft);
            t.setTickInterval(1);
            fallingTiles.put(ft, t);
        }
    }

    private void checkIfMovesPossible() {
        if(gameOver) {
            return;
        }
        possibleMoves.clear();
        for (int a = 0; a < field.length; a++) {
            for (int b = 0; b < field[0].length; b++) {
                if (field[b][a] == 0) {
                    continue;
                }
                ArrayList<Point> currentMove = new ArrayList<>();
                currentMove.add(new Point(b, a));
                findNeighbours(new boolean[20][20], currentMove, b, a);
                if (currentMove.size() >= 3) {
                    possibleMoves.add(currentMove);
                }
            }
        }
    }

    @Override
    public void mouseMoved(int fromX, int fromY, int toX, int toY) {
        int arrX = (toX - xOffset) / 40;
        int arrY = (toY - yOffset) / 40;
        updateHighlights(arrX, arrY);
    }

    private void updateHighlights(int arrX, int arrY) {
        if(gameOver) {
            return;
        }
        highlightedTiles.clear();
        if (arrX < field[0].length && arrY < field.length && arrX >= 0 && arrY >= 0) {
            if (field[arrY][arrX] == 0) {
                return;
            }
            findNeighbours(new boolean[20][20], highlightedTiles, arrX, arrY);
            highlightedTiles.add(new Point(arrX, arrY));
//            if(!(highlightedTiles.size() >= 3)) {
//                highlightedTiles.clear();
//            }
        }
    }

    @Override
    public void mouseReleased(int button, int x, int y) {
        if (!begun || paused || gameOver) {
            return;
        }
        int arrX = (x - xOffset) / 40;
        int arrY = (y - yOffset) / 40;
        if (button == Input.MOUSE_LEFT_BUTTON) {
            popGemGroup(arrX, arrY);
        }
        /*
        Cheat / Debug Util
        else if(button == Input.MOUSE_RIGHT_BUTTON) {
            if (arrX < field[0].length && arrY < field.length && arrX >= 0 && arrY >= 0) {
                if (field[arrY][arrX] < 8) {
                    field[arrY][arrX] += 1;
                } else {
                    field[arrY][arrX] = 1;
                }
            }
        } else if(button == Input.MOUSE_MIDDLE_BUTTON) {
            if (arrX < field[0].length && arrY < field.length && arrX >= 0 && arrY >= 0) {
                    field[arrY][arrX] = 0;
                    dropColumn(arrX);
            }
        }
        */
    }

    private void popGemGroup(int arrX, int arrY) {
        if (arrX < field[0].length && arrY < field.length && arrX >= 0 && arrY >= 0) {
            if(field[arrY][arrX] == 0) {
                return;
            }
            if (field[arrY][arrX] >= 1) {
                match3Gems(arrX, arrY);
                updateHighlights(arrX, arrY);
            }
            checkIfMovesPossible();
            if (possibleMoves.isEmpty()) {
                reshuffleField();
            }
            showHint = false;
            hintTicker.setTicks(0);
            hintTiles.clear();
        }
    }

    private void reshuffleField() {
        if(gameOver) {
            return;
        }
        for (int a = 0; a < field.length; a++) {
            for (int b = 0; b < field[0].length; b++) {
                if(field[b][a] == 0) {
                    continue;
                }
                droppedTiles.add(new DroppedTile(field[b][a] - 1, a, b));
                field[b][a] = 0;
            }
        }
        for (int a = 0; a < field.length; a++) {
            fillColumn(a);
        }
        checkIfMovesPossible();
    }

    private void match3Gems(int x, int y) {
        ArrayList<Point> gems = new ArrayList<>();
        gems.add(new Point(x, y));
        findNeighbours(new boolean[20][20], gems, x, y);
        ArrayList<Integer> cols = new ArrayList<>();
        if (gems.size() >= 3) {
            //SCORE INCREASE
            score += ((gems.size()+level)*differentGemCount)*1.1+level*differentGemCount*2.3;
            for (Point p : gems) {
                droppedTiles.add(new DroppedTile(field[p.y][p.x] - 1, p.x, p.y));
                field[p.y][p.x] = 0;
                if (!cols.contains(p.x)) {
                    cols.add(p.x);
                }
            }
        }
        for (Integer i : cols) {
            dropColumn(i);
        }
    }

    private void dropColumn(int col, int speed) {
        if (droppingColumns[col]) {
            return;
        }
        droppingColumns[col] = true;
        checkFieldForZero = true;
        int row = -1;
        int dist = 0;
        for (int a = field.length - 1; a >= 0; a--) {
            if (field[a][col] == 0) {
                if (row <= 0) {
                    row = a;
                }
                ++dist;
            } else {
                if (row >= 0) {
                    break;
                }
            }
        }
        if (row == -1) {
            return;
        }
        for (int a = row; a >= 0; a--) {
            if (a < dist) {
                FallingTile ft = new FallingTile(generator.nextInt(differentGemCount), xOffset + col * 40, yOffset + (a - dist) * 40, yOffset + a * 40, true, speed);
                field[a][col] = 0;
                Ticker t = new Ticker(ft);
                fallingTiles.put(ft, t);
            } else {
                FallingTile ft = new FallingTile(field[a - dist][col] - 1, xOffset + col * 40, yOffset + (a - dist) * 40, yOffset + a * 40, false, speed);
                Ticker t = new Ticker(ft);
                fallingTiles.put(ft, t);
                //field[a][col] = field[a - dist][col];
                field[a][col] = 0;
            }
        }
    }

    /*
    Fills a column with gems, by dropping gems over
     */
    private void dropColumn(int col) {
        dropColumn(col, 0);
    }

    private void findNeighbours(boolean[][] visited, ArrayList<Point> gems, int x, int y) {
        if(field[y][x] == 0) {
            gems.clear();
            return;
        }
        visited[y][x] = true;
        if (x > 0) {
            if (field[y][x] == field[y][x - 1] && !visited[y][x - 1]) {
                gems.add(new Point(x - 1, y));
                findNeighbours(visited, gems, x - 1, y);
            }
        }
        if (x < field[0].length - 1) {
            if (field[y][x] == field[y][x + 1] && !visited[y][x + 1]) {
                gems.add(new Point(x + 1, y));
                findNeighbours(visited, gems, x + 1, y);
            }
        }
        if (y > 0) {
            if (field[y][x] == field[y - 1][x] && !visited[y - 1][x]) {
                gems.add(new Point(x, y - 1));
                findNeighbours(visited, gems, x, y - 1);
            }
        }
        if (y < field.length - 1) {
            if (field[y][x] == field[y + 1][x] && !visited[y + 1][x]) {
                gems.add(new Point(x, y + 1));
                findNeighbours(visited, gems, x, y + 1);
            }
        }
    }

    private class FallingTile implements Ticker.Tickable {

        private int gemID;

        private int speed = 0;

        private float opacity = 1.0f;

        private Point coords;
        private Point targetCoords;

        private float distance;
        private float travelled = 0f;

        boolean done = false;

        boolean fade;

        public FallingTile(int gemID, int fromX, int fromY, int toY, boolean fade) {
            this.gemID = gemID;
            coords = new Point(fromX, fromY);
            targetCoords = new Point(fromX, toY);
            distance = Math.abs(fromY - toY);
            this.fade = fade;
            if (fade) {
                opacity = 1.0f;
            } else {
                opacity = 0f;
            }
        }

        public FallingTile(int gemID, int fromX, int fromY, int toY, boolean fade, int speed) {
            this(gemID, fromX, fromY, toY, fade);
            this.speed = speed;
        }

        @Override
        public void onTick() {
            speed += 2;
            travelled += speed;
            if (fade) {
                opacity = 1.0f - travelled / distance;
            }
            if (speed + coords.y < targetCoords.y) {
                coords.y += speed;
            } else {
                coords.y = targetCoords.y;
                opacity = 0;
                done = true;
            }
        }

        public int getGemID() {
            return gemID;
        }

        public int getSpeed() {
            return speed;
        }

        public Point getTargetCoords() {
            return targetCoords;
        }

        public float getOpacity() {
            return opacity;
        }

        public Point getCoords() {
            return coords;
        }

        public boolean isDone() {
            return done;
        }
    }

    private class DroppedTile implements Ticker.Tickable {

        boolean clockwise;

        private Point position;

        private int angle;

        private Ticker ticker = new Ticker(this);

        private int orgY;

        private int gemID;

        public DroppedTile(int gemID, int arrayX, int arrayY) {
            this.gemID = gemID;
            clockwise = generator.nextBoolean();
            position = new Point(xOffset + 40 * arrayX, yOffset + 40 * arrayY);
            ticker.setTickInterval(1);
            orgY = position.y;
        }

        @Override
        public void onTick() {
            angle += 10;
            double b = Math.pow(angle - 50, 2) * 0.01 - 50;
            this.position.x += ((clockwise ? 1 : -1) * angle) / 9;
            position.y = orgY + (int) b;
        }

        public int getAngle() {
            return angle;
        }

        public float getOpacity() {
            return 1 - (float) angle / 90.f;
        }

        public boolean isDone() {
            return angle >= 90;
        }

        public Ticker getTicker() {
            return ticker;
        }
    }

    private class HintTile implements Ticker.Tickable {

        private Point pos;

        private float opacity = 0f;

        private boolean increase = true;

        private int idle = 20;

        private Ticker ticker = new Ticker(this);

        private int gemID;

        public HintTile(int gemID, int arrX, int arrY) {
            this.gemID = gemID;
            pos = new Point(xOffset + arrX * 40, yOffset + arrY * 40);
            ticker.setTickInterval(1);
        }

        public float getOpacity() {
            return opacity;
        }

        @Override
        public void onTick() {
            if (increase) {
                if (opacity < 1) {
                    opacity += 0.1f;
                } else {
                    increase = false;
                    idle = 20;
                }
            } else {
                if (idle > 0) {
                    --idle;
                    return;
                }
                if (opacity > 0) {
                    opacity -= 0.1f;
                } else {
                    increase = true;
                }
            }
        }

        public Point getPos() {
            return pos;
        }

        public Ticker getTicker() {
            return ticker;
        }

        public int getGemID() {
            return gemID;
        }
    }

}
