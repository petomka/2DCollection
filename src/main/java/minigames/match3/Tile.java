package minigames.match3;

import org.newdawn.slick.*;
import org.newdawn.slick.Image;

import java.awt.*;

/**
 * Created by Benedikt on 25.05.2017.
 */
public class Tile {

    private int gemID;
    private Point coords;

    boolean active = true;

    public Tile(int gemID, int x, int y) {
        if (gemID == 0) {
            gemID = 0;
        }
        this.gemID = gemID;
        coords = new Point(x, y);
    }

    public void render(org.newdawn.slick.Graphics g, Image[] images) {
        if (gemID >= images.length || !active) {
            return;
        }
        images[gemID].draw(350 + coords.x * 40, 35 + coords.y * 40);
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getGemID() {
        return gemID;
    }

    public void setGemID(int gemID) {
        this.gemID = gemID;
    }

    public int getX() {
        return coords.x;
    }

    public int getY() {
        return coords.y;
    }

    public Point getCoords() {
        return coords;
    }
}
