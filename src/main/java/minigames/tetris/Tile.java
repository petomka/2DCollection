package minigames.tetris;

/**
 * Created by Benedikt on 24.05.2017.
 */
public class Tile {

    boolean[][] box;
    int color;

    public Tile(ETile e) {
        color = e.color;
        box = e.getBox().clone();
    }

    public void turnClockwise() {
        box = rotateCW(box.clone());
    }

    public void turnCounterClockwise() {
        box = rotateCCW(box.clone());
    }

    /**
     * ClockWise rotation
     * @param mat the matrix to rotate
     * @return the rotatet matrix
     */
    private boolean[][] rotateCW(boolean[][] mat) {
        final int M = mat.length;
        final int N = mat[0].length;
        boolean[][] ret = new boolean[N][M];
        for (int r = 0; r < M; r++) {
            for (int c = 0; c < N; c++) {
                ret[c][M-1-r] = mat[r][c];
            }
        }
        return ret;
    }

    /**
     * CountClockWise rotation
     * @param mat the matrix to rotate
     * @return the rotated matrix
     */
    private boolean[][] rotateCCW(boolean[][] mat) {
        final int M = mat.length;
        final int N = mat[0].length;
        boolean[][] ret = new boolean[N][M];
        for(int r = 0; r < M; r++) {
            for(int c = 0; c < N; c++) {
                ret[N-1-c][r] = mat[r][c];
            }
        }
        return ret;
    }

    public boolean[][] getBox() {
        return box;
    }

    public int getColor() {
        return color;
    }
}
