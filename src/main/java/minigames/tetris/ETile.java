package minigames.tetris;

/**
 * Defines valid tiles. Used in Tile.java
 */
public enum ETile {

    I(new boolean[][]{{true},
            {true},
            {true},
            {true}}, 0),

    L(new boolean[][]{{true, false},
            {true, false},
            {true, true}}, 1),

    Lr(new boolean[][]{{false, true}, {false, true}, {true, true}}, 2),

    O(new boolean[][]{{true, true}, {true, true}}, 3),

    S(new boolean[][]{{false, true, true}, {true, true, false}}, 4),

    T(new boolean[][]{ {true, true, true}, {false, true, false}}, 5),

    Sr(new boolean[][]{{true, true, false}, {false, true, true}}, 6);

    boolean[][] box;

    int color;

    ETile(boolean[][] box, int color) {
        this.box = box;
        this.color = color;
    }

    public boolean[][] getBox() {
        return box;
    }

    public int getColorID() {
        return color;
    }
}
