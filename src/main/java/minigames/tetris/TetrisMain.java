package minigames.tetris;

import org.newdawn.slick.*;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.state.StateBasedGame;
import states.MiniGameState;
import util.Point3;
import util.Ticker;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Benedikt on 24.05.2017.
 */
public class TetrisMain extends MiniGameState implements Ticker.Tickable {

    public static final int ID = 3;

    private boolean[][] field;
    private ArrayList<Integer> completedLines = new ArrayList<>();
    private ArrayList<Point3> fieldColor;

    private Image background, tileGreen, tileDarkBlue, tileOrange, tilePurple, tileRed, tileTurqoise, tileYellow, tileWhite;

    private Image[] imageID;

    private Point currentTilePos = new Point(3, 0);
    private Tile currentTile = null, nextTile = null;

    private Animation completeAnimation = new Animation();
    private Mover mover = new Mover();

    private Ticker tileGravity = new Ticker(this), fastGravity = new Ticker(this), completeAnimationTicker = new Ticker(completeAnimation), moverTicker = new Ticker(mover);

    private int interval = 40;

    private boolean begun = false, interrupted = false, gameOver = false;

    private int xOffset = 450, yOffset = 40;

    private int score = 0, scoreLines = 0, level = 0;

    public TetrisMain() {
        super(ID);
        tileGravity.setTickInterval(interval);
        tileGravity.setTicks(0);
        fastGravity.setTickInterval(1);
        completeAnimationTicker.setTickInterval(5);
        moverTicker.setTickInterval(3);
        moverTicker.setDelay(8);
    }

    @Override
    public void onTick() {
        gravityCurrentTile();
    }

    @Override
    public void initG(GameContainer gc, StateBasedGame game) throws SlickException {
        field = new boolean[20][10];
        fieldColor = new ArrayList<>();
        background = new Image("img/tetris/moscowBackgroundUI.png");
        tileGreen = new Image("img/tetris/tile.png");
        tileDarkBlue = new Image("img/tetris/tileDarkBlue.png");
        tileOrange = new Image("img/tetris/tileOrange.png");
        tilePurple = new Image("img/tetris/tilePurple.png");
        tileRed = new Image("img/tetris/tileRed.png");
        tileTurqoise = new Image("img/tetris/tileTurqoise.png");
        tileYellow = new Image("img/tetris/tileYellow.png");
        tileWhite = new Image("img/tetris/tileWhite.png");
        imageID = new Image[]{tileDarkBlue, tileYellow, tileRed, tileGreen, tileOrange, tileTurqoise, tilePurple, tileWhite};
    }

    @Override
    public void updateG(GameContainer gc, StateBasedGame game, int i) throws SlickException {
        if (!begun) {
            if (in.isKeyPressed(Input.KEY_SPACE)) {
                begun = true;
                spawnNewTile();
            }
        }
        if(gameOver) {
            if(in.isKeyPressed(Input.KEY_SPACE)) {
                resetMinigame();
                begun = true;
                spawnNewTile();
            }
        }
        if (in.isKeyPressed(Input.KEY_SPACE) && begun && !gameOver) {
            paused = !paused;
        }
        if (completedLines.size() > 0 && !paused && begun && !gameOver) {
            interrupted = true;
            completeAnimationTicker.tick();
        }
        if (paused || interrupted || !begun || gameOver) {
            return;
        }
        if (in.isKeyPressed(Input.KEY_RIGHT) || (in.isKeyPressed(Input.KEY_D))) {
            moveCurrent(true);
        }
        if (in.isKeyPressed(Input.KEY_LEFT) || (in.isKeyPressed(Input.KEY_A))) {
            moveCurrent(false);
        }
        if(in.isKeyDown(Input.KEY_LEFT) || in.isKeyDown(Input.KEY_A)) {
            mover.right = false;
            moverTicker.tick();
        } else if(in.isKeyDown(Input.KEY_RIGHT) || in.isKeyDown(Input.KEY_D)) {
            mover.right = true;
            moverTicker.tick();
        } else {
            moverTicker.setTicks(0);
            moverTicker.setDelay(8);
        }
        if (in.isKeyPressed(Input.KEY_UP) || (in.isKeyPressed(Input.KEY_W))) {
            rotateCurrent(true);
        }
        if (in.isKeyPressed(Input.KEY_C)) {
            rotateCurrent(false);
        }
        if (in.isKeyPressed(Input.KEY_R)) {
            while (gravityCurrentTile()) {
                score += 1;
            }
        }
        if (in.isKeyDown(Input.KEY_S) || in.isKeyDown(Input.KEY_DOWN)) {
            fastGravity.tick();
            score += 1;
        } else {
            tileGravity.tick();
        }
    }

    @Override
    public void renderG(GameContainer gc, StateBasedGame game, Graphics g) throws SlickException {
        background.draw(0, 0);
        if(!gameOver) {
            renderTile(currentTile, currentTilePos.x, currentTilePos.y);
        }
        if(nextTile != null) {
            renderTile(nextTile, 3 - nextTile.getBox()[0].length + 14, 8);
        }
        for (Point3 p : fieldColor) {
            if (p.z < imageID.length) {
                imageID[p.z].draw(xOffset + p.x * 40, yOffset + p.y * 40);
            }
        }
        if (!begun) {
            g.setColor(Color.white);
            g.drawString("Drücke LEER um das Spiel zu starten!", 480, 440);
        }
        if (paused && begun && !gameOver) {
            g.setColor(Color.white);
            g.drawString("Pausiert!", 610, 440);
        }
        if(gameOver) {
            g.setColor(Color.red);
            g.drawString("Game Over! Drücke LEER um neuzustarten!", 470, 440);
        }
        g.setColor(Color.white);
        g.drawString("Score: " + score, 1000, 220);
        g.drawString("Lines: " + scoreLines, 1000, 240);
    }

    @Override
    public void resetMinigame() {
        begun = false;
        interrupted = false;
        paused = false;
        currentTile = null;
        nextTile = null;
        gameOver = false;
        interval = 40;
        field = new boolean[20][10];
        score = 0;
        scoreLines = 0;
        completedLines.clear();
        fieldColor.clear();
        tileGravity.setTickInterval(interval);
        tileGravity.setTicks(0);
        in.resetInputTransform();
    }

    Tile help1 = new Tile(ETile.S);
    Tile help2 = new Tile(ETile.Sr);
    Tile help3 = new Tile(ETile.T);
    Tile help4 = new Tile(ETile.I);
    Tile help5 = new Tile(ETile.O);
    Tile help6 = new Tile(ETile.L);
    Tile help7 = new Tile(ETile.Lr);

    @Override
    public void renderHelpScreenG(Graphics g) {
        g.drawString("Ziel in \"Tetris\" ist es, möglichst viele Punkte zu sammeln. Du bekommst Punkte, wenn du eine Reihe komplett \n" +
                "mit Steinen aufgefüllt hast. Eine komplette Reihe löst sich auf, und wenn du mehrere Reihen auf einmal komplettierst \n" +
                "bekommst du mehr Punkte.\n\n" +
                "Diese Steine gibt es:", 110, 170);
        renderTile(help1, -8, 8);
        renderTile(help2, -4, 8);
        renderTile(help3, 0, 8);
        renderTile(help4, 4, 8);
        renderTile(help5, 6, 8);
        renderTile(help6, 9, 8);
        renderTile(help7, 12, 8);

        wasd.draw(200, 580);
        g.setColor(Color.yellow);
        g.drawString("Mit \"W\" drehst du deinen Stein im Uhrzeigersinn. Mit A und D bewegst du den Stein\n" +
                "nach links und rechts. Wenn du \"S\" gedrückt hältst, fällt der Stein schneller und\n" +
                "du bekommst Bonuspunkte. Mit \"R\" fällt der Stein sofort ganz nach unten. \n" +
                "Alternativ kannst du auch mit den Pfeiltasten steuern (\"R\" bleibt). Mit der \n" +
                "Leertaste kannst du das Spiel jederzeit pausieren.", 400, 580);
    }

    private void renderTile(Tile t, int xPos, int yPos) {
        if (t == null) {
            return;
        }
        if (t.getColor() < 0 || t.getColor() >= imageID.length) {
            return;
        }
        for (int y = 0; y < t.getBox().length; y++) {
            for (int x = 0; x < t.getBox()[0].length; x++) {
                if (t.getBox()[y][x]) {
                    imageID[t.getColor()].draw(xOffset + xPos * 40 + x * 40, yOffset + yPos * 40 + y * 40);
                }
            }
        }
    }

    private void moveCurrent(boolean right) {
        if ((currentTilePos.x + currentTile.getBox()[0].length > 9 && right) || (currentTilePos.x == 0 && !right)) {
            return;
        }
        if (currentTilePos.x < 10 - currentTile.getBox()[0].length && right) {
            for (int a = 0; a < currentTile.getBox().length; a++) {
                for (int b = 0; b < currentTile.getBox()[0].length; b++) {
                    if (currentTile.getBox()[a][b] && field[currentTilePos.y + a][currentTilePos.x + b + 1]) {
                        return;
                    }
                }
            }
        }
        if (currentTilePos.x > 0 && !right) {
            for (int a = 0; a < currentTile.getBox().length; a++) {
                for (int b = 0; b < currentTile.getBox()[0].length; b++) {
                    if (currentTile.getBox()[a][b] && field[currentTilePos.y + a][currentTilePos.x + b - 1]) {
                        return;
                    }
                }
            }
        }
        currentTilePos.x += right ? 1 : -1;

    }

    private void rotateCurrent(boolean cw) {
        if (cw) {
            currentTile.turnClockwise();
            for (int a = 0; a < currentTile.getBox().length; a++) {
                for (int b = 0; b < currentTile.getBox()[0].length; b++) {
                    if (currentTilePos.x + b < field[0].length && currentTilePos.y + a < field.length) {
                        if (currentTile.getBox()[a][b] && field[currentTilePos.y + a][currentTilePos.x + b]) {
                            currentTile.turnCounterClockwise();
                            return;
                        }
                    }
                }
            }
        } else {
            currentTile.turnCounterClockwise();
            currentTile.turnClockwise();
            for (int a = 0; a < currentTile.getBox().length; a++) {
                for (int b = 0; b < currentTile.getBox()[0].length; b++) {
                    if (currentTilePos.x + b < field[0].length && currentTilePos.y + a < field.length) {
                        if (currentTile.getBox()[a][b] && field[currentTilePos.y + a][currentTilePos.x + b]) {
                            currentTile.turnClockwise();
                            return;
                        }
                    }
                }
            }
        }
        if (currentTilePos.x + (currentTile.getBox()[0].length) > 10) {
            currentTilePos.x = 10 - currentTile.getBox()[0].length;
        }
        if (currentTilePos.y + (currentTile.getBox().length) > 20) {
            currentTilePos.y = 20 - currentTile.getBox().length;
        }
    }

    private boolean gravityCurrentTile() {
        if (currentTilePos.y + currentTile.getBox().length < 20) {
            outer:
            {
                for (int y = 0; y < currentTile.getBox().length; y++) {
                    for (int x = 0; x < currentTile.getBox()[0].length; x++) {
                        if (field[y + currentTilePos.y + 1][x + currentTilePos.x] && currentTile.getBox()[y][x]) {
                            break outer;
                        }
                    }
                }
                currentTilePos.y += 1;
                return true;
            }
        }

        Point3[] toAdd = new Point3[4];
        int ind = 0;
        for (int y = 0; y < currentTile.getBox().length; y++) {
            for (int x = 0; x < currentTile.getBox()[0].length; x++) {
                if (currentTile.getBox()[y][x]) {
                    toAdd[ind++] = new Point3(x + currentTilePos.x, y + currentTilePos.y, currentTile.getColor());
                    field[y + currentTilePos.y][x + currentTilePos.x] = true;
                }
            }
        }
        fieldColor.addAll(Arrays.asList(toAdd));
        checkLines();
        spawnNewTile();
        return false;
    }

    private void spawnNewTile() {
        currentTilePos.x = 3;
        currentTilePos.y = 0;
        if (nextTile == null) {
            nextTile = new Tile(ETile.values()[generator.nextInt(ETile.values().length)]);
            currentTile = new Tile(ETile.values()[generator.nextInt(ETile.values().length)]);
            return;
        }
        if(field[currentTilePos.y][currentTilePos.x]) {
            currentTile = null;
            gameOver();
            return;
        }
        currentTile = nextTile;
        nextTile = new Tile(ETile.values()[generator.nextInt(ETile.values().length)]);
    }

    private void gameOver() {
        gameOver = true;
    }

    private void checkLines() {
        for (int a = 0; a < field.length; a++) {
            inner:
            {
                for (int b = 0; b < field[0].length; b++) {
                    if (!field[a][b]) {
                        break inner;
                    } else if (b == 9) {
                        if (!completedLines.contains(a)) {
                            completedLines.add(a);
                        }
                    }
                }
            }
        }
    }

    private class Mover implements Ticker.Tickable {

        boolean right;

        @Override
        public void onTick() {
            moveCurrent(right);
        }

    }

    private class Animation implements Ticker.Tickable {

        private boolean visible = true;
        private boolean done = false;
        private int i = 0;

        @Override
        public void onTick() {
            ++i;
            if (i == 4) {
                i = 0;
                done = true;
                interrupted = false;
                ArrayList<Point3> sift = new ArrayList<>();
                for (Integer integer : completedLines) {
                    if (integer > 0) {
                        for (int a = integer; a > 0; a--) {
                            for (int b = 0; b < field[0].length; b++) {
                                if (a == 1) {
                                    field[a][b] = false;
                                } else {
                                    field[a][b] = field[a - 1][b];
                                }
                            }
                        }
                    }
                    ArrayList<Point3> rem = new ArrayList<>();
                    for (Point3 p : fieldColor) {
                        if (p.y == integer) {
                            rem.add(p);
                        } else if (p.y < integer) {
                            sift.add(p);
                        }
                    }
                    fieldColor.removeAll(rem);
                    for (Point3 p : sift) {
                        p.y += 1;
                    }
                    sift.clear();
                }
                if (completedLines.size() == 1) {
                    score += 40 * (level + 1);
                } else if (completedLines.size() == 2) {
                    score += 100 * (level + 1);
                } else if (completedLines.size() == 3) {
                    score += 300 * (level + 1);
                } else {
                    score += 1200 * (level + 1);
                }
                scoreLines += completedLines.size();
                completedLines.clear();
            } else {
                for (Integer integer : completedLines) {
                    for (Point3 p : fieldColor) {
                        if (p.y == integer) {
                            p.z = imageID.length - 1;
                        }
                    }
                }
            }
        }

        public boolean isDone() {
            return done;
        }

        public void setDone(boolean done) {
            this.done = done;
        }
    }
}
