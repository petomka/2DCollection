package minigames.asteroids;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import main.Main;
import org.newdawn.slick.Image;

import java.util.concurrent.ThreadLocalRandom;

@Getter
@Setter
public class Asteroid implements AsteroidsOject {

	@Getter
	private static Image[] asteroidID;

	private static Image asteroid1s3,
			asteroid1s2,
			asteroid1s1;

	private Image asteroidImage;

	@SneakyThrows
	public static void init() {
		asteroid1s3 = new Image("img/asteroids/asteroid1s4.png");
		asteroid1s2 = new Image("img/asteroids/asteroid1s3.png");
		asteroid1s1 = new Image("img/asteroids/asteroid1s2.png");
		asteroidID = new Image[]{asteroid1s1, asteroid1s2, asteroid1s3};
	}

	private float x;
	private float y;
	private float vX;
	private float vY;
	private float rotation;
	private int size;

	public Asteroid(float x, float y, int directionAngle, int size) {
		this(x, y, (float) Math.sin(2 * Math.PI * (directionAngle / 360f)), (float) Math.cos(2 * Math.PI * (directionAngle / 360f)), size);
	}

	public Asteroid(float x, float y, float vX, float vY, int size) {
		if(size <= 0 || size-1 >= asteroidID.length) {
			throw new RuntimeException("Asteroid size is out of bounds: " + size);
		}
		asteroidImage = asteroidID[size - 1].copy();
		this.x = x;
		this.y = y;
		this.vX = vX;
		this.vY = vY;
		this.size = size;
		rotation = ThreadLocalRandom.current().nextInt(10) > 5 ? -1f : 1f;
	}

	public void move() {
		x += vX * AsteroidsMain.getInstance().getAccFactor() * 30 / size;
		y += vY * AsteroidsMain.getInstance().getAccFactor() * 30 / size;
		if (rotation > 0f) {
			rotation += 13f / size * 0.33;
			if (rotation > 360) {
				rotation = rotation - 359;
			}
		} else {
			rotation -= 13f / size * 0.33;
			if (rotation < -360) {
				rotation = rotation + 361;
			}
		}
		if (getX() < 0 - getWidth()) {
			setX(getX() + Main.getWindowWidth());
		}
		if (getX() > Main.getWindowWidth()) {
			setX(getX() - Main.getWindowWidth());
		}
		if (getY() < 0 - getHeight()) {
			setY(getY() + Main.getWindowHeight());
		}
		if (getY() > Main.getWindowHeight()) {
			setY(getY() - Main.getWindowHeight());
		}
		
	}

	public int getRadius() {
		return 16 * size;
	}

	public int getWidth() {
		return asteroidImage.getWidth();
	}

	public int getHeight() {
		return asteroidImage.getHeight();
	}

	public void rotate(float angle) {
		rotation += angle;
		asteroidImage.rotate(angle);
	}

	public void draw(float x, float y) {
		asteroidImage.draw(x, y);
	}

}
