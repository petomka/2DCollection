package minigames.asteroids;

import org.newdawn.slick.Color;

public interface AsteroidsOject {

	float getX();

	float getY();

	void setX(float x);

	void setY(float y);

	int getWidth();

	int getHeight();

	float getRotation();

	void rotate(float angle);

	void draw(float x, float y);

	default void draw(float x, float y, Color color) {
		draw(x, y);
	}

}
