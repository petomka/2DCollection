package minigames.asteroids;

import lombok.Getter;
import lombok.SneakyThrows;
import org.newdawn.slick.Image;

@Getter
public class Explosion {

	private static Image[] explosion;

	public static int getExplosionLength() {
		return explosion.length;
	}

	@SneakyThrows
	public static void init() {
		explosion = new Image[10];
		for (int i = 0; i < explosion.length; i++) {
			explosion[i] = new Image("img/asteroids/explosion" + i + ".png");
		}
	}

	private int x;
	private int y;
	private int delay = 0;
	private int frameIndex = 0;

	public Explosion(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void nextFrame() {
		if (delay == 2) {
			++frameIndex;
			delay = 0;
		} else {
			++delay;
		}
	}

	public Image getCurrentFrame() {
		if (frameIndex < explosion.length) {
			return explosion[frameIndex];
		}
		return explosion[0];
	}
}
