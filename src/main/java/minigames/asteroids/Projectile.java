package minigames.asteroids;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.newdawn.slick.Color;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Vector2f;

import java.awt.*;

@Getter
@Setter
public class Projectile {

	private static Image shotProjectile;

	@SneakyThrows
	public static void init() {
		shotProjectile = new Image("img/asteroids/shot.png");
	}

	float x;
	float y;
	float vX;
	float vY;
	int dur = 0;
	boolean player;

	public Projectile(int x, int y, float rotation, boolean player, int dur) {
		this(x, y, (float) Math.sin(2 * Math.PI * (rotation / 360f)), (float) Math.cos(2 * Math.PI * (rotation / 360f)), player, dur);
	}

	public Projectile(int x, int y, Vector2f target, boolean player, int dur) {
		this(x, y, new Point((int)target.x, (int)target.y), player, dur);
	}

	public Projectile(int x, int y, Point target, boolean player, int dur) {
		Vector2f dir = new Vector2f(target.x-x, target.y -y).getNormal();

		this.x = x;
		this.y = y;
		this.vX = dir.x;
		this.vY = dir.y;
		this.player = player;
		this.dur = dur;
		if(player) {
			this.vX *= 1.3;
			this.vY *= 1.3;
		}
	}

	public Projectile(int x, int y, float vX, float vY, boolean player, int dur) {
		this.x = x;
		this.y = y;
		this.vX = vX;
		this.vY = vY;
		this.player = player;
		this.dur = dur;
		if (player) {
			this.vX *= 1.3;
			this.vY *= 1.3;
		}
	}

	void move() {
		dur -= 1;
		x += vX * AsteroidsMain.getInstance().getAccFactor() * 40;
		y += vY * AsteroidsMain.getInstance().getAccFactor() * 40;
	}

	public int getWidth() {
		return shotProjectile.getWidth();
	}

	public int getHeight() {
		return shotProjectile.getHeight();
	}

	public void draw(float x, float y) {
		shotProjectile.draw(x, y);
	}

	public void draw(float x, float y, Color color) {
		shotProjectile.draw(x, y, color);
	}
}
