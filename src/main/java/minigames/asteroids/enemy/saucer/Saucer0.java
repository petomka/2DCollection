package minigames.asteroids.enemy.saucer;

import com.google.common.collect.Lists;
import minigames.asteroids.AsteroidsMain;
import minigames.asteroids.Projectile;

import java.util.List;

public class Saucer0 extends Saucer {

	public Saucer0(float x, float y, float vX, float vY) {
		super(x, y, vX, vY);
	}

	@Override
	protected void initShootTicker() {
		shootTicker.setTickInterval(55);
	}

	@Override
	public List<Projectile> shoot() {
		List<Projectile> hostileShots = Lists.newArrayList();
		hostileShots.add(new Projectile((int) x, (int) y, AsteroidsMain.getInstance().getPlayerShip().getCenterLocation(),
				false, 200));
		return hostileShots;
	}

}
