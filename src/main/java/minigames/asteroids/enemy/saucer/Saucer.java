package minigames.asteroids.enemy.saucer;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import main.Main;
import minigames.asteroids.AsteroidsMain;
import minigames.asteroids.AsteroidsOject;
import minigames.asteroids.Projectile;
import minigames.asteroids.player.Spaceship;
import org.newdawn.slick.Image;
import util.Ticker;

import java.awt.*;
import java.util.List;

@Getter
@Setter
public abstract class Saucer implements Ticker.Tickable, AsteroidsOject {

	protected float x;
	protected float y;
	protected float vX;
	protected float vY;
	protected float rotation;
	protected Ticker shootTicker = new Ticker(this);
	protected Image saucerCopy;

	@Getter
	private static Image saucerImage;

	@SneakyThrows
	public static void init() {
		saucerImage = new Image("img/asteroids/saucer.png");
	}

	public Saucer(float x, float y, float vX, float vY) {
		initShootTicker(); //shootTicker.setTickInterval(70 - shipLevel * 15);
		this.x = x;
		this.y = y;
		this.vX = vX;
		this.vY = vY;
		this.saucerCopy = Saucer.saucerImage.copy();
	}

	@Override
	public void onTick() {
		AsteroidsMain.getInstance().onHostileShoot(shoot());
	}

	public int getWidth() {
		return saucerImage.getWidth();
	}

	public int getHeight() {
		return saucerImage.getHeight();
	}

	public void rotate(float angle) {
		rotation += angle;
		saucerCopy.rotate(angle);
	}

	public void draw(float x, float y) {
		saucerCopy.draw(x, y);
	}

	protected float calcPlayerDirectionAngle() {
		float directionAngle;
		Spaceship ship = AsteroidsMain.getInstance().getPlayerShip();
		Point playerPosition = ship.getLocation();
		if (playerPosition.y - y != 0) {
			directionAngle = (float) Math.atan((Math.abs((playerPosition.x + ship.getWidth() / 2f) - x)) /
					(Math.abs((playerPosition.y + ship.getHeight() / 2f) - y))) * 180f / (float) Math.PI;
			if (playerPosition.y < y) {
				directionAngle = directionAngle + 180;
			}
			if (playerPosition.x > x && playerPosition.y < y) {
				directionAngle = -directionAngle;
			}
			if (playerPosition.x < x && playerPosition.y > y) {
				directionAngle = -directionAngle;
			}
		} else {
			directionAngle = playerPosition.x - x > 0 ? -90 : 90;
		}
		return directionAngle;
	}

	protected abstract void initShootTicker();

	public abstract List<Projectile> shoot();

	public void move() {
		if (vX > 0) {
			rotation += 1.5;
		} else {
			rotation -= 1.5;
		}
		x += vX * 3;
		Point playerPosition = AsteroidsMain.getInstance().getPlayerShip().getCenterLocation();
		if (Math.abs(playerPosition.y - y) > 400) {
			if (playerPosition.y > y) {
				y += 2.5;
			} else {
				y -= 2.5;
			}
		}
		if (getX() < 0 - getWidth()) {
			setX(getX() + Main.getWindowWidth());
		}
		if (getX() > Main.getWindowWidth()) {
			setX(getX() - Main.getWindowWidth());
		}
		if (getY() < 0 - getHeight()) {
			setY(getY() + Main.getWindowHeight());
		}
		if (getY() > Main.getWindowHeight()) {
			setY(getY() - Main.getWindowHeight());
		}
	}
}
