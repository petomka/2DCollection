package minigames.asteroids.enemy.saucer;

import com.google.common.collect.Lists;
import minigames.asteroids.Projectile;

import java.util.List;

public class Saucer3 extends Saucer {

	public Saucer3(float x, float y, float vX, float vY) {
		super(x, y, vX, vY);
	}

	@Override
	protected void initShootTicker() {
		shootTicker.setTickInterval(35);
	}

	@Override
	public List<Projectile> shoot() {
		List<Projectile> hostileShots = Lists.newArrayList();
		float directionAngle = calcPlayerDirectionAngle();
		hostileShots.add(new Projectile((int) x, (int) y, directionAngle, false, 25));
		for (int i = -17; i < 18; i++) {
			hostileShots.add(new Projectile((int) x, (int) y, directionAngle + i * 5, false, 75));
		}
		return hostileShots;
	}

}
