package minigames.asteroids.enemy.saucer;

import com.google.common.collect.Lists;
import minigames.asteroids.AsteroidsMain;
import minigames.asteroids.Projectile;

import java.awt.*;
import java.util.List;

public class Saucer1 extends Saucer {

	public Saucer1(float x, float y, float vX, float vY) {
		super(x, y, vX, vY);
	}

	@Override
	protected void initShootTicker() {
		shootTicker.setTickInterval(55);
	}

	@Override
	public List<Projectile> shoot() {
		List<Projectile> hostileShots = Lists.newArrayList();
		Point playerCenter = AsteroidsMain.getInstance().getPlayerShip().getCenterLocation();
		float directionAngle = calcPlayerDirectionAngle();
		hostileShots.add(new Projectile((int) x, (int) y, playerCenter, false, 200));
		hostileShots.add(new Projectile((int) x, (int) y, directionAngle + 5, false, 200));
		hostileShots.add(new Projectile((int) x, (int) y, directionAngle - 5, false, 200));
		return hostileShots;
	}

}
