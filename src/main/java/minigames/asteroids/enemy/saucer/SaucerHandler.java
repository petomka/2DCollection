package minigames.asteroids.enemy.saucer;

import com.google.common.collect.ImmutableMap;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;

import java.lang.reflect.Constructor;
import java.util.Map;

@UtilityClass
public class SaucerHandler {

	private Class<?> defaultSaucer = Saucer3.class;

	private Map<Integer, Class<?>> saucerLevels = ImmutableMap.of(
			0, Saucer0.class,
			1, Saucer1.class,
			2, Saucer2.class,
			3, Saucer3.class
	);

	@SneakyThrows
	public Saucer getSaucer(int playerLevel, float x, float y, float vX, float vY) {
		Constructor<?> constructor = saucerLevels.getOrDefault(playerLevel, defaultSaucer)
				.getConstructor(float.class, float.class, float.class, float.class);
		return (Saucer) constructor.newInstance(x, y, vX, vY);
	}

}
