package minigames.asteroids.enemy.saucer;

import com.google.common.collect.Lists;
import minigames.asteroids.Projectile;

import java.util.List;

public class Saucer2 extends Saucer {

	public Saucer2(float x, float y, float vX, float vY) {
		super(x, y, vX, vY);
	}

	@Override
	protected void initShootTicker() {
		shootTicker.setTickInterval(45);
	}

	@Override
	public List<Projectile> shoot() {
		List<Projectile> hostileShots = Lists.newArrayList();
		float directionAngle = calcPlayerDirectionAngle();
		hostileShots.add(new Projectile((int) x, (int) y, directionAngle, false, 200));
		hostileShots.add(new Projectile((int) x, (int) y, directionAngle + 2, false, 200));
		hostileShots.add(new Projectile((int) x, (int) y, directionAngle - 2, false, 200));
		hostileShots.add(new Projectile((int) x, (int) y, directionAngle + 4, false, 200));
		hostileShots.add(new Projectile((int) x, (int) y, directionAngle - 4, false, 200));
		hostileShots.add(new Projectile((int) x, (int) y, directionAngle + 6, false, 200));
		hostileShots.add(new Projectile((int) x, (int) y, directionAngle - 6, false, 200));
		return hostileShots;
	}

}
