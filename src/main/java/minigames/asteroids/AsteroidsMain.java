package minigames.asteroids;

import com.google.common.collect.Lists;
import gui.Button;
import lombok.Getter;
import main.Main;
import minigames.asteroids.enemy.saucer.Saucer;
import minigames.asteroids.enemy.saucer.SaucerHandler;
import minigames.asteroids.player.Spaceship;
import minigames.asteroids.player.SpaceshipHandler;
import org.newdawn.slick.Color;
import org.newdawn.slick.*;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.state.StateBasedGame;
import states.MiniGameState;
import util.Ticker;

import java.awt.*;
import java.util.List;

/**
 * Created by Benedikt on 29.05.2017.
 */
public class AsteroidsMain extends MiniGameState implements Ticker.Tickable {

	public static final int ID = 6;

	@Getter
	private static AsteroidsMain instance;

	private Image shield, background;

	boolean inputThrust = false;

	private Image coinImage;

	private Button closeShopButton, buyLifeButton, buyShipButton;

	public AsteroidsMain() {
		super(ID);
		AsteroidsMain.instance = this;
	}

	private Ticker invulnerableTicker = new Ticker(this);

	@Getter
	private float accFactor = 0.2f, dragFactor = 0.01f;

	private boolean invulnerable = true;

	@Getter
	private Spaceship playerShip = SpaceshipHandler.getStarterShip();

	@Getter
	private List<Projectile> playerShots = Lists.newArrayList();

	private List<Asteroid> asteroids = Lists.newArrayList();

	@Getter
	private List<Projectile> hostileShots = Lists.newArrayList();

	private List<Saucer> saucers = Lists.newArrayList();

	private List<Explosion> explosions = Lists.newArrayList();

	private int level = 0;

	private int lives = 3;

	private int score = 0;

	private int coins = 0;

	private int boughtLifes = 0;

	private boolean begun = false;

	private boolean gameOver = false;

	private boolean deathPause = false;

	private boolean inShop = false;

	@Override
	public void onTick() {
		invulnerable = false;
	}

	@Override
	public void initG(GameContainer gc, StateBasedGame game) throws SlickException {
		background = new Image("img/asteroids/background.png");

		shield = new Image("img/asteroids/shield.png");
		coinImage = new Image("img/asteroids/coin.png");

		playerShip.setX(Main.getWindowWidth() / 2f);
		playerShip.setY(Main.getWindowHeight() / 2f);

		Explosion.init();
		Saucer.init();
		Asteroid.init();
		Projectile.init();

		invulnerableTicker.setTickInterval(250);

		closeShopButton = new Button(gc, escapeImage, 110, 110);
		closeShopButton.setEnabled(false);
		closeShopButton.setMouseOverImage(escapeImageHover);

		Image buyImage = new Image("img/asteroids/buyButtonScaled.png");

		buyLifeButton = new Button(gc, buyImage, 750, 335);
		buyLifeButton.setEnabled(false);

		buyShipButton = new Button(gc, buyImage, 750, 435);
		buyShipButton.setEnabled(false);

	}

	@Override
	public void updateG(GameContainer gc, StateBasedGame game, int i) throws SlickException {
		List<Explosion> explosionsToRemove = Lists.newArrayList();
		for (Explosion e : explosions) {
			e.nextFrame();
			if (e.getFrameIndex() >= Explosion.getExplosionLength()) {
				explosionsToRemove.add(e);
			}
		}
		explosions.removeAll(explosionsToRemove);
		if (in.isKeyPressed(Input.KEY_P) && begun && !gameOver) {
			paused = !paused;
		}
		if (closeShopButton.isPressed()) {
			inShop = false;
			closeShopButton.setPressed(false);
			closeShopButton.setEnabled(inShop);
			help.setEnabled(!inShop);
			escape.setEnabled(!inShop);
			//werden in renderShop() aktiviert
			buyLifeButton.setEnabled(false);
			buyShipButton.setEnabled(false);
		}
		if (in.isKeyPressed(Input.KEY_I) && !paused && !gameOver && begun) {
			inShop = !inShop;
			closeShopButton.setEnabled(inShop);
			help.setEnabled(!inShop);
			escape.setEnabled(!inShop);
			//werden in renderShop() aktiviert
			buyLifeButton.setEnabled(false);
			buyShipButton.setEnabled(false);
		}
		if (buyLifeButton.isPressed()) {
			buyLifeButton.setPressed(false);
			if (coins >= boughtLifes + 1) {
				coins -= (boughtLifes + 1);
				if (boughtLifes == 0) {
					boughtLifes = 1;
				} else {
					boughtLifes *= 3;
				}
				++lives;
			}
		}
		if (buyShipButton.isPressed()) {
			buyShipButton.setPressed(false);
			if (SpaceshipHandler.hasNextLevel(playerShip)) {
				Spaceship newShip = SpaceshipHandler.levelUpfrom(playerShip);
				if (coins >= newShip.getPrice()) {
					coins -= newShip.getPrice();
					playerShip = newShip;
				}
			}
		}
		if (!begun || gameOver || deathPause) {
			if (in.isKeyPressed(Input.KEY_SPACE)) {
				begun = true;
				paused = false;
				if (gameOver) {
					resetMinigame();
					gameOver = false;
					begun = true;
				}
				if (deathPause) {
					deathPause = false;
				}
				return;
			}
		}
		if (!begun || gameOver || paused || deathPause || inShop) {
			return;
		}

		List<Projectile> projectilesToRemove = calcProjectilesToRemove(playerShots);

		List<Saucer> saucersToRemove = Lists.newArrayList();
		outer:
		for (Saucer saucer : saucers) {
			saucer.getShootTicker().tick();
			saucer.move();
			for (Projectile shotProjectile : playerShots) {
				if (intersects((int) saucer.getX(), (int) saucer.getY(),
						saucer.getWidth() / 2 + shotProjectile.getWidth() / 2,
						new Point((int) shotProjectile.x, (int) shotProjectile.y))) {
					saucersToRemove.add(saucer);
					projectilesToRemove.add(shotProjectile);
					score += 300 + level * 10;
					explosions.add(new Explosion((int) saucer.getX(), (int) saucer.getY()));
					if (level >= 3) {
						coins += generator.nextInt(level - 2) + 1;
					}
					continue outer;
				}
			}
		}


		playerShots.removeAll(projectilesToRemove);
		projectilesToRemove.clear();

		projectilesToRemove = calcProjectilesToRemove(hostileShots);

		hostileShots.removeAll(projectilesToRemove);
		projectilesToRemove.clear();

		for (Asteroid asteroid : asteroids) {
			asteroid.move();
		}

		saucers.removeAll(saucersToRemove);

		if (!invulnerable) {
			Point playerPosition = playerShip.getLocation();
			for (Asteroid asteroid : asteroids) {
				if (intersects((int) asteroid.getX(), (int) asteroid.getY(), asteroid.getRadius(), playerPosition, new Point(playerPosition.x + playerShip.getWidth(), playerPosition.y + playerShip.getHeight()),
						new Point((int) playerShip.getX() + playerShip.getWidth() / 2, (int) playerShip.getY() + playerShip.getHeight() / 2))) {
					explosions.add(new Explosion(playerPosition.x + playerShip.getWidth() / 2, playerPosition.y + playerShip.getHeight() / 2));
					loseLife();
				}
			}
			for (Projectile hostilesShots : hostileShots) {
				if (intersects((int) hostilesShots.x, (int) hostilesShots.y, 5, playerPosition, new Point(playerPosition.x + playerShip.getWidth(), playerPosition.y + playerShip.getHeight()),
						new Point(playerPosition.x + playerShip.getWidth() / 2, playerPosition.y + playerShip.getHeight() / 2))) {
					explosions.add(new Explosion(playerPosition.x + playerShip.getWidth() / 2, playerPosition.y + playerShip.getHeight() / 2));
					loseLife();
				}
			}
		}

		List<Asteroid> asteroidsToRemove = Lists.newArrayList();
		List<Asteroid> asteroidsToAdd = Lists.newArrayList();
		projectilesToRemove.clear();

		outer:
		for (Asteroid asteroid : asteroids) {
			for (Projectile shotProjectile : playerShots) {
				if (intersects((int) asteroid.getX(), (int) asteroid.getY(), asteroid.getRadius() + shotProjectile.getWidth() / 2,
						new Point((int) shotProjectile.x, (int) shotProjectile.y))) {
					if (asteroid.getSize() > 1) {
						asteroidsToAdd.add(new Asteroid(asteroid.getX(), asteroid.getY(), (int) asteroid.getRotation() + 30, asteroid.getSize() - 1));
						asteroidsToAdd.add(new Asteroid(asteroid.getX(), asteroid.getY(), (int) asteroid.getRotation() - 30, asteroid.getSize() - 1));
					} else {
						explosions.add(new Explosion((int) asteroid.getX(), (int) asteroid.getY()));
					}
					score += 12 / asteroid.getSize() * (5 + level) + level * 4;
					if (level >= 3) {
						if (generator.nextInt(400) <= 20) {
							Saucer toAdd = SaucerHandler.getSaucer(playerShip.getSpaceshipLevel(),
									generator.nextInt(2) > 1 ? 0f : 1300f,
									generator.nextInt(2) > 1 ? 200 : 600,
									generator.nextInt(10) > 5 ? 0.7f : -0.7f, 0f
							);
							saucers.add(toAdd);
						}
					}
					asteroidsToRemove.add(asteroid);
					projectilesToRemove.add(shotProjectile);
					continue outer;
				}
			}
		}
		playerShots.removeAll(projectilesToRemove);
		asteroids.removeAll(asteroidsToRemove);
		asteroids.addAll(asteroidsToAdd);
		playerShip.move();
		if (in.isKeyDown(Input.KEY_W) || in.isKeyDown(Input.KEY_UP)) {
			playerShip.setMoving(true);
			inputThrust = true;
			playerShip.accelerate();
		} else {
			playerShip.setMoving(false);
		}
		if (in.isKeyDown(Input.KEY_D) || in.isKeyDown(Input.KEY_RIGHT)) {
			playerShip.rotate(6);
		}
		if (in.isKeyDown(Input.KEY_A) || in.isKeyDown(Input.KEY_LEFT)) {
			playerShip.rotate(-6);
		}
		if (in.isKeyPressed(Input.KEY_SPACE) || (playerShip.isRapidFire() && in.isKeyDown(Input.KEY_SPACE))) {
			shoot();
			playerShip.setShooting(true);
		} else {
			playerShip.setShooting(false);
		}
		spawnNewWave();
		invulnerableTicker.tick();
	}

	public void onHostileShoot(List<Projectile> projectiles) {
		hostileShots.addAll(projectiles);
	}

	private List<Projectile> calcProjectilesToRemove(List<Projectile> shots) {
		List<Projectile> projectilesToRemove = Lists.newArrayList();
		for (Projectile shotProjectile : shots) {
			shotProjectile.move();
			if (shotProjectile.getX() > 1300) {
				shotProjectile.setX(-shotProjectile.getWidth());
				shotProjectile.setDur(shotProjectile.getDur() - 40);
			}
			if (shotProjectile.getX() < -shotProjectile.getWidth()) {
				shotProjectile.setX(1300);
				shotProjectile.setDur(shotProjectile.getDur() - 40);
			}
			if (shotProjectile.getY() > 864) {
				shotProjectile.setY(-shotProjectile.getHeight());
				shotProjectile.setDur(shotProjectile.getDur() - 40);
			}
			if (shotProjectile.getY() < -shotProjectile.getHeight()) {
				shotProjectile.setY(864);
				shotProjectile.setDur(shotProjectile.getDur() - 40);
			}
			if (shotProjectile.getDur() <= 0) {
				projectilesToRemove.add(shotProjectile);
			}
		}
		return projectilesToRemove;
	}

	private void rotateRenderable(AsteroidsOject asteroidsOject) {
		asteroidsOject.rotate(asteroidsOject.getRotation());
		asteroidsOject.draw(asteroidsOject.getX() - asteroidsOject.getWidth() / 2f, asteroidsOject.getY() - asteroidsOject.getHeight() / 2f);
		if (asteroidsOject.getX() < asteroidsOject.getWidth() / 2) {
			asteroidsOject.draw(1300 + asteroidsOject.getX() - asteroidsOject.getWidth() / 2f, asteroidsOject.getY() - asteroidsOject.getHeight() / 2f);
		} else if (asteroidsOject.getX() > 1300 - asteroidsOject.getWidth()) {
			asteroidsOject.draw(asteroidsOject.getX() - asteroidsOject.getWidth() - 1300, asteroidsOject.getY() - asteroidsOject.getHeight() / 2f);
		}
		if (asteroidsOject.getY() < asteroidsOject.getHeight() / 2) {
			asteroidsOject.draw(asteroidsOject.getX() - asteroidsOject.getWidth() / 2f, 864 + asteroidsOject.getY() - asteroidsOject.getHeight() / 2f);
		} else if (asteroidsOject.getY() > 864 - asteroidsOject.getHeight()) {
			asteroidsOject.draw(asteroidsOject.getX() - asteroidsOject.getWidth(), asteroidsOject.getY() - asteroidsOject.getHeight() / 2f - 864);
		}
		asteroidsOject.rotate(-asteroidsOject.getRotation());
	}

	@Override
	public void renderG(GameContainer gc, StateBasedGame game, Graphics g) throws SlickException {
		background.draw(0, 0);
		for (Projectile shotProjectile : playerShots) {
			shotProjectile.draw(shotProjectile.x - shotProjectile.getWidth() / 2f, shotProjectile.y - shotProjectile.getHeight() / 2f);
		}
		for (Projectile shotProjectile : hostileShots) {
			shotProjectile.draw(shotProjectile.x - shotProjectile.getWidth() / 2f, shotProjectile.y - shotProjectile.getHeight() / 2f, new Color(0xff4444));
		}
		for (Saucer saucer : saucers) {
			rotateRenderable(saucer);
		}
		for (Asteroid asteroid : asteroids) {
			rotateRenderable(asteroid);
		}
		if (!gameOver && !deathPause) {
			Point playerPosition = playerShip.getLocation();
			int maxStarship = playerShip.getMaxDimensionLength();
			playerShip.draw(playerPosition.x, playerPosition.y);
			if (playerPosition.x < maxStarship) {
				playerShip.draw(1300 + playerPosition.x, playerPosition.y);

			} else if (playerPosition.x > 1300 - maxStarship) {
				playerShip.draw(playerPosition.x - 1300, playerPosition.y);

			}
			if (playerPosition.y < maxStarship) {
				playerShip.draw(playerPosition.x, 864 + playerPosition.y);

			} else if (playerPosition.y > 864 - maxStarship) {
				playerShip.draw(playerPosition.x, playerPosition.y - 864);

			}
			if (invulnerable) {
				shield.draw(playerPosition.x + (Math.abs(playerShip.getWidth()) - Math.abs(shield.getWidth())) / 2f,
						playerPosition.y + (Math.abs(playerShip.getHeight()) - Math.abs(shield.getHeight())) / 2f);
			}
		}
		for (Explosion e : explosions) {
			e.getCurrentFrame().draw(e.getX() - e.getCurrentFrame().getWidth() / 2f, e.getY() - e.getCurrentFrame().getHeight() / 2f);
		}
		g.drawString("Leben: " + lives, 30, 820);
		g.drawString("Level: " + level, 200, 820);
		g.drawString("Score: " + score, 370, 820);
		g.drawString(coins + " x ", 1200, 820);
		coinImage.draw(1210, 799);
		if (!begun) {
			g.drawString("Drücke FEUER um zu starten!", 550, 412);
		}
		if (paused && begun && !gameOver) {
			g.drawString("Pausiert!", 580, 412);
		}
		if (!paused && deathPause) {
			g.drawString("Drücke FEUER um weiterzuspielen!", 530, 412);
		}
		if (gameOver) {
			g.setColor(Color.red);
			g.drawString("Game Over! Drücke FEUER um neuzustarten!", 500, 412);
			g.setColor(Color.white);
			return;
		}
		if (inShop) {
			renderShopScreen(gc, g);
		}
	}

	@Override
	public void resetMinigame() {
		playerShip = SpaceshipHandler.getStarterShip();
		playerShip.reset();
		boughtLifes = 0;
		gameOver = false;
		begun = false;
		paused = false;
		deathPause = false;
		inShop = false;
		score = 0;
		coins = 0;
		asteroids.clear();
		playerShots.clear();
		hostileShots.clear();
		explosions.clear();
		saucers.clear();
		level = 0;
		lives = 3;
		invulnerable = true;
		invulnerableTicker.setTicks(0);
	}

	@Override
	public void renderHelpScreenG(Graphics g) {
		g.drawString("Das Ziel in \"Asteroids\" ist es, solange wie möglich zu überleben. Du weichst den Asteroiden aus und versuchst, diese \n" +
				"abzuschießen. Ab Level 3 können feindliche Raumschiffe erscheinen, die dich mit hoher Präzision abschießen zu versuchen.\n" +
				"Du musst sie zuerst abschießen!\n\n" +
				"Jeder Schuss kostet dich eine bestimmte Anzahl deiner Punktzahl, wenn du schon schießt, dann triff also auch.\n\n" +
				"Das sind die Asteroiden:", 110, 170);
		for (int i = 0; i < Asteroid.getAsteroidID().length; i++) {
			Asteroid.getAsteroidID()[i].draw(110 +
					(i > 0 ? Asteroid.getAsteroidID()[i - 1].getWidth() + i * Asteroid.getAsteroidID()[i].getWidth() : 0), 330);
		}
		g.setColor(Color.orange);
		g.drawString("Ein größerer Asteroid teilt sich wenn er abgeschossen wird in \n" +
				"zwei der nächstkleineren auf. Wenn ein Asteroid keinen kleineren\n" +
				"Nachfolger hat, explodiert er.", 550, 335);

		g.setColor(Color.white);
		g.drawString("Das sind die gegnerischen Raumschiffe:", 110, 470);
		Saucer.getSaucerImage().draw(240, 510);
		g.setColor(Color.red);
		g.drawString("Gewöhnlich bewegen sie sich nur in eine Richtung, aber wenn du\n" +
				"zu weit nach oben oder unten abweichst, korrigieren sie ihren Kurs.\n" +
				"Sie schießen außerdem sehr präzise!", 550, 515);

		g.setColor(Color.white);
		g.drawString("Münzen    bekommst du, wenn du Raumschiffe abschießt. Diese kannst du im Shop (Drücke \"I\") ausgeben.", 110, 620);
		coinImage.draw(150, 598);

		g.setColor(Color.white);
	}

	private boolean intersects(int circleCenterX, int circleCenterY, int circleRad, Point... rectP) {
		for (Point aRectP : rectP) {
			if (circleEq(Math.abs(circleCenterX), Math.abs(circleCenterY), Math.abs(circleRad), aRectP) <= 0) {
				return true;
			}
		}
		return false;
	}

	private double circleEq(int cX, int cY, int r, Point p) {
		return (Math.pow(Math.abs(p.x) - cX, 2) + Math.pow(Math.abs(p.y) - cY, 2)) - Math.pow(r, 2);
	}


	private void spawnNewWave() {
		if (!asteroids.isEmpty()) {
			return;
		}
		score += level * 300;
		++level;
		invulnerable = true;
		invulnerableTicker.setTicks(0);
		for (int i = 0; i < (level * 3 / 6) + 1; i++) {
			asteroids.add(new Asteroid(generator.nextInt(1300), generator.nextInt(864), generator.nextInt(359), 3));
		}
	}

	private void shoot() {
		List<Projectile> shotsToAdd = playerShip.shoot();
		int scoreMalus = (playerShip.getSpaceshipLevel() + 1) * 6 * shotsToAdd.size();
		if(score - scoreMalus < 0) {
			score = 0;
		} else {
			score -= scoreMalus;
		}
		playerShots.addAll(shotsToAdd);
	}

	private void loseLife() {
		--lives;
		if (lives == 0) {
			gameOver = true;
			return;
		}
		playerShip.reset();
		invulnerable = true;
		invulnerableTicker.setTicks(0);
		deathPause = true;
	}

	private void renderShopScreen(GameContainer gc, Graphics g) {
		helpWindow.draw(0, 0);
		closeShopButton.render(gc, g);
		buyLifeButton.setEnabled(true);
		g.drawString("Zusatzleben kaufen: " + (boughtLifes + 1) + " x ", 450, 350);
		coinImage.draw(640, 329);
		buyLifeButton.render(gc, g);
		if (SpaceshipHandler.hasNextLevel(playerShip)) {
			Spaceship nextShip = SpaceshipHandler.levelUpfrom(playerShip);
			buyShipButton.setEnabled(true);
			g.drawString("Neues Schiff kaufen: " + nextShip.getPrice() + " x ", 450, 450);
			coinImage.draw(650, 429);
			buyShipButton.render(gc, g);
		}
	}


}
