package minigames.asteroids.player;

import com.google.common.collect.ImmutableList;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;

import java.lang.reflect.Constructor;
import java.util.List;

@UtilityClass
public class SpaceshipHandler {

	private Class<?> defaultSpaceship = Spaceship3.class;

	private List<Class<?>> spaceShips = ImmutableList.of(
			Spaceship0.class,
			Spaceship1.class,
			Spaceship2.class,
			Spaceship3.class
	);

	public static boolean hasNextLevel(Spaceship spaceship) {
		return spaceShips.size() - 1 > spaceship.getSpaceshipLevel();
	}

	public static Spaceship getStarterShip() {
		return levelUpfrom(-1);
	}

	@SneakyThrows
	public static Spaceship levelUpfrom(int level) {
		Constructor<?> newShipConstructor;
		if (level < -1 || level - 1 >= spaceShips.size()) {
			newShipConstructor = defaultSpaceship.getConstructor();
		} else {
			newShipConstructor = spaceShips.get(level + 1).getConstructor();
		}
		return (Spaceship) newShipConstructor.newInstance();
	}


	@SneakyThrows
	public static Spaceship levelUpfrom(Spaceship oldShip) {
		if (oldShip.getSpaceshipLevel() + 2 > spaceShips.size()) {
			return oldShip;
		}
		Spaceship ship = levelUpfrom(oldShip.getSpaceshipLevel());
		ship.setY(oldShip.getY());
		ship.setX(oldShip.getX());
		return ship;
	}

}
