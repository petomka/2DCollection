package minigames.asteroids.player;

import com.google.common.collect.Lists;
import minigames.asteroids.Projectile;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import java.awt.*;
import java.util.List;

public class Spaceship1 extends Spaceship {


	private static Image starship2Idle;

	private static Image starship2Thrust;

	static {
		try {
			starship2Idle = new Image("img/asteroids/starship2Idle.png");
			starship2Thrust = new Image("img/asteroids/starship2Thrust.png");
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

	public Spaceship1() {
		super(starship2Thrust, starship2Thrust, starship2Idle, starship2Idle, 1, 15);
	}

	@Override
	public List<Projectile> shoot() {
		List<Projectile> shots = Lists.newArrayList();
		Point center = getCenterLocation();
		shots.add(new Projectile(center.x, center.y, -getRotation() + 175, true, 120));
		shots.add(new Projectile(center.x, center.y, -getRotation() + 180, true, 120));
		shots.add(new Projectile(center.x, center.y, -getRotation() + 185, true, 120));
		return shots;
	}
}
