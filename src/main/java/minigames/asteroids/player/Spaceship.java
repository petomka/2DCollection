package minigames.asteroids.player;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import main.Main;
import minigames.asteroids.AsteroidsMain;
import minigames.asteroids.AsteroidsOject;
import minigames.asteroids.Projectile;
import org.newdawn.slick.Image;

import java.awt.*;
import java.util.List;

@Getter
@Setter
@RequiredArgsConstructor
public abstract class Spaceship implements AsteroidsOject {

	protected final Image thrustImage;
	protected final Image shootingThrust;
	protected final Image idleImage;
	protected final Image shootingIdle;
	protected final int spaceshipLevel;
	protected final int price;

	private float x;
	private float y;
	private float vX;
	private float vY;
	private float rotation;

	protected boolean shooting;
	protected boolean moving;

	@Override
	public int getWidth() {
		return idleImage.getWidth();
	}

	@Override
	public int getHeight() {
		return idleImage.getHeight();
	}

	@Override
	public void rotate(float angle) {
		if (rotation >= 360) {
			rotation -= 360;
		}
		if (rotation <= 0) {
			rotation += 359;
		}
		rotation += angle;
		thrustImage.rotate(angle);
		if (!thrustImage.equals(shootingThrust)) {
			shootingThrust.rotate(angle);
		}
		idleImage.rotate(angle);
		if (!idleImage.equals(shootingIdle)) {
			shootingIdle.rotate(angle);
		}
	}

	public Point getLocation() {
		return new Point((int) x, (int) y);
	}

	public Point getCenterLocation() {
		Point center = getLocation();
		center.x += getWidth() / 2;
		center.y += getHeight() / 2;
		return center;
	}

	public void move() {
		setX(getX() + getVX());
		setY(getY() + getVY());
		setVX(getVX() * AsteroidsMain.getInstance().getDragFactor() * 98);
		setVY(getVY() * AsteroidsMain.getInstance().getDragFactor() * 98);
		if (getX() < 0 - getWidth()) {
			setX(getX() + 1300);
		}
		if (getX() > 1300) {
			setX(getX() - 1300);
		}
		if (getY() < 0 - getHeight()) {
			setY(getY() + Main.getWindowHeight());
		}
		if (getY() > Main.getWindowHeight()) {
			setY(getY() - Main.getWindowHeight());
		}
	}

	public void accelerate() {
		float xN = (float) Math.sin(2 * Math.PI * (rotation / 360f));
		float yN = (float) Math.cos(2 * Math.PI * (rotation / 360f));

		if (Math.abs(getVX()) < 7) {
			vX += xN * AsteroidsMain.getInstance().getAccFactor() * 5;
		}
		if (Math.abs(getVY()) < 7) {
			vY -= yN * AsteroidsMain.getInstance().getAccFactor() * 5;
		}
	}

	@Override
	public void draw(float x, float y) {
		if (shooting && moving) {
			shootingThrust.draw(x, y);
			return;
		}
		if (shooting) {
			shootingIdle.draw(x, y);
			return;
		}
		if (moving) {
			thrustImage.draw(x, y);
			return;
		}
		idleImage.draw(x, y);
	}

	public void reset() {
		setX(Main.getWindowWidth() / 2f);
		setY(Main.getWindowHeight() / 2f);
		setVX(0f);
		setVY(0f);
		setRotation(0);
		shootingIdle.setRotation(0);
		idleImage.setRotation(0);
		thrustImage.setRotation(0);
		shootingThrust.setRotation(0);
		setShooting(false);
		setMoving(false);
	}

	public abstract List<Projectile> shoot();

	public boolean isRapidFire() {
		return false;
	}

	public int getMaxDimensionLength() {
		return Math.max(
				Math.max(thrustImage.getWidth(), Math.max(shootingThrust.getWidth(),
						Math.max(idleImage.getWidth(), shootingIdle.getWidth()))),
				Math.max(thrustImage.getHeight(), Math.max(shootingThrust.getHeight(),
						Math.max(idleImage.getHeight(), shootingIdle.getHeight())))
		);
	}

}
