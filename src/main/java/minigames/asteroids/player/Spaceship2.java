package minigames.asteroids.player;

import com.google.common.collect.Lists;
import minigames.asteroids.Projectile;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import java.awt.*;
import java.util.List;

public class Spaceship2 extends Spaceship {

	private static Image starship3Idle;
	private static Image starship3Thrust;

	private int shipShotCount = 0;

	static {
		try {
			starship3Idle = new Image("img/asteroids/starship3Idle.png");
			starship3Thrust = new Image("img/asteroids/starship3Thrust.png");
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

	public Spaceship2() {
		super(starship3Thrust, starship3Thrust, starship3Idle, starship3Idle, 2, 75);
	}

	@Override
	public List<Projectile> shoot() {
		List<Projectile> shots = Lists.newArrayList();
		Point center = getCenterLocation();
		if (shipShotCount == 2) {
			double xP1 = Math.sin(2 * Math.PI * ((-getRotation() + 90) / 360f));
			double yP1 = Math.cos(2 * Math.PI * ((-getRotation() + 90) / 360f));
			shots.add(new Projectile(center.x + (int) (xP1 * getWidth() / 2), center.y +
					(int) (yP1 * getWidth() / 2), -getRotation() + 180, true, 120));
		} else if (shipShotCount == 4) {
			double xP2 = Math.sin(2 * Math.PI * ((-getRotation() - 90) / 360f));
			double yP2 = Math.cos(2 * Math.PI * ((-getRotation() - 90) / 360f));
			shots.add(new Projectile(center.x + (int) (xP2 * getWidth() / 2), center.y +
					(int) (yP2 * getWidth() / 2), -getRotation() + 180, true, 120));
			shipShotCount = 0;
		}
		++shipShotCount;
		return shots;
	}

	@Override
	public boolean isRapidFire() {
		return true;
	}
}
