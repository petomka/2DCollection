package minigames.asteroids.player;

import com.google.common.collect.Lists;
import minigames.asteroids.Projectile;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import java.awt.*;
import java.util.List;

public class Spaceship3 extends Spaceship {

	private static Image starship4Idle;
	private static Image starship4Thrust;
	private static Image starship4IdleShot;
	private static Image starship4ThrustShot;
	private static Image thrustShootFlipped;
	private static Image idleShootFlipped;

	private int shipShotCount = 0;
	private int shotInterval = 3;

	static {
		try {
			starship4Idle = new Image("img/asteroids/starship4Idle.png");
			starship4Thrust = new Image("img/asteroids/starship4Thrust.png");
			starship4IdleShot = new Image("img/asteroids/starship4IdleShot.png");
			starship4ThrustShot = new Image("img/asteroids/starship4ThrustShot.png");
			thrustShootFlipped = starship4ThrustShot.getFlippedCopy(true, false);
			idleShootFlipped = starship4IdleShot.getFlippedCopy(true, false);
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

	public Spaceship3() {
		super(starship4Thrust, starship4ThrustShot, starship4Idle, starship4IdleShot, 3, 450);
	}

	@Override
	public List<Projectile> shoot() {
		List<Projectile> shots = Lists.newArrayList();
		Point center = getCenterLocation();
		if (shipShotCount == shotInterval) {
			double xP1 = Math.sin(2 * Math.PI * ((-getRotation() + 90) / 360f));
			double yP1 = Math.cos(2 * Math.PI * ((-getRotation() + 90) / 360f));
			shots.add(new Projectile(center.x + (int) (xP1 * getWidth() / 4), center.y + (int) (yP1 * getWidth() / 4), -getRotation() + 174, true, 120));
			shots.add(new Projectile(center.x + (int) (xP1 * getWidth() / 4), center.y + (int) (yP1 * getWidth() / 4), -getRotation() + 177, true, 120));
			shots.add(new Projectile(center.x + (int) (xP1 * getWidth() / 4), center.y + (int) (yP1 * getWidth() / 4), -getRotation() + 180, true, 120));
			shots.add(new Projectile(center.x + (int) (xP1 * getWidth() / 4), center.y + (int) (yP1 * getWidth() / 4), -getRotation() + 183, true, 120));
			shots.add(new Projectile(center.x + (int) (xP1 * getWidth() / 4), center.y + (int) (yP1 * getWidth() / 4), -getRotation() + 186, true, 120));
		} else if (shipShotCount == shotInterval*2) {
			double xP2 = Math.sin(2 * Math.PI * ((-getRotation() - 90) / 360f));
			double yP2 = Math.cos(2 * Math.PI * ((-getRotation() - 90) / 360f));
			shots.add(new Projectile(center.x + (int) (xP2 * getWidth() / 4), center.y + (int) (yP2 * getWidth() / 4), -getRotation() + 174, true, 120));
			shots.add(new Projectile(center.x + (int) (xP2 * getWidth() / 4), center.y + (int) (yP2 * getWidth() / 4), -getRotation() + 177, true, 120));
			shots.add(new Projectile(center.x + (int) (xP2 * getWidth() / 4), center.y + (int) (yP2 * getWidth() / 4), -getRotation() + 180, true, 120));
			shots.add(new Projectile(center.x + (int) (xP2 * getWidth() / 4), center.y + (int) (yP2 * getWidth() / 4), -getRotation() + 183, true, 120));
			shots.add(new Projectile(center.x + (int) (xP2 * getWidth() / 4), center.y + (int) (yP2 * getWidth() / 4), -getRotation() + 186, true, 120));
			shipShotCount = 0;
		}
		++shipShotCount;
		return shots;
	}

	@Override
	public void rotate(float angle) {
		super.rotate(angle);
		idleShootFlipped.rotate(angle);
		thrustShootFlipped.rotate(angle);
	}

	@Override
	public void reset() {
		super.reset();
		idleShootFlipped.setRotation(0);
		thrustShootFlipped.setRotation(0);
	}

	@Override
	public void draw(float x, float y) {
		Image thrustShot = shootingThrust;
		Image idleShot = shootingIdle;
		if(shipShotCount >= shotInterval) {
			idleShot = idleShootFlipped;
			thrustShot = thrustShootFlipped;
		}
		if (shooting && moving) {
			thrustShot.draw(x, y);
			return;
		}
		if (shooting) {
			idleShot.draw(x, y);
			return;
		}
		if (moving) {
			thrustImage.draw(x, y);
			return;
		}
		idleImage.draw(x, y);
	}

	@Override
	public boolean isRapidFire() {
		return true;
	}
}
