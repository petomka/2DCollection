package minigames.asteroids.player;

import com.google.common.collect.Lists;
import minigames.asteroids.Projectile;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import java.awt.*;
import java.util.List;

public class Spaceship0 extends Spaceship {

	private static Image starship1Idle, starship1Thrust;

	static  {
		try {
			starship1Idle = new Image("img/asteroids/starshipRedIdle.png");
			starship1Thrust = new Image("img/asteroids/starshipRedThrust.png");
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

	public Spaceship0() {
		super(starship1Thrust, starship1Thrust, starship1Idle, starship1Idle, 0, 0);
	}

	@Override
	public List<Projectile> shoot() {
		List<Projectile> shots = Lists.newArrayList();
		Point center = getCenterLocation();
		shots.add(new Projectile(center.x, center.y, -getRotation() + 180, true, 120));
		return shots;
	}
}
