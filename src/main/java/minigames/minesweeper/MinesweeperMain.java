package minigames.minesweeper;

import gui.Button;
import org.newdawn.slick.*;
import org.newdawn.slick.state.StateBasedGame;
import states.MiniGameState;

/**
 * Probably removed in future except someone likes this
 */
public class MinesweeperMain extends MiniGameState {

    public static final int ID = 5;

    private Image background, edge;

    private boolean[][] bombs = null;


    private Button increaseX, decreaseX, increaseY, decreaseY, start;

    private boolean begun = false;

    int xSize =15, minXSize = 5, maxXSize = 50, ySize = 15, minYSize = 5, maxYSize = 40;

    private Image[] fieldID;

    int xOffset = 250, yOffset = 35;

    public MinesweeperMain() {
        super(ID);
    }

    @Override
    public void initG(GameContainer gc, StateBasedGame game) throws SlickException {
        background = new Image("img/minesweeper/background.png");
        edge = new Image("img/minesweeper/edge.png");
        Image add = new Image("img/minesweeper/addButtonScaled.png");
        Image rem = new Image("img/minesweeper/removeButtonScaled.png");
        Image play = new Image("img/minesweeper/playButtonScaled.png");
        Image playHover = new Image("img/minesweeper/playButtonScaledHover.png");
        increaseX = new Button(gc, add, 700, 300);
        decreaseX = new Button(gc, rem, 550, 300);

        increaseY = new Button(gc, add, 700, 400);
        decreaseY = new Button(gc, rem, 550, 400);

        start = new Button(gc, play, 625, 500);
        start.setMouseOverImage(playHover);

        fieldID = new Image[14];
        Image field = new Image("img/minesweeper/field.png");
        Image fieldEmpty = new Image("img/minesweeper/fieldEmpty.png");
        Image fieldQM = new Image("img/minesweeper/fieldQM.png");
        Image fieldFlag = new Image("img/minesweeper/fieldFlag.png");
        Image fieldBomb = new Image("img/minesweeper/fieldBomb.png");
        Image fieldBombRed = new Image("img/minesweeper/fieldBombRed.png");
        for(int a = 0; a < 8; a++) {
            Image fieldA = new Image("img/minesweeper/field" +(a+1)+".png");
            fieldID[a+6] = fieldA;
        }
        fieldID[0] = field;
        fieldID[1] = fieldEmpty;
        fieldID[2] = fieldQM;
        fieldID[3] = fieldFlag;
        fieldID[4] = fieldBomb;
        fieldID[5] = fieldBombRed;
    }

    @Override
    public void updateG(GameContainer gc, StateBasedGame game, int i) throws SlickException {
        if(!begun && bombs == null) {
            if(in.isKeyPressed(Input.KEY_LEFT) || in.isKeyPressed(Input.KEY_A) || decreaseX.isPressed()) {
                if(xSize > minXSize) {
                    --xSize;
                }
                decreaseX.setPressed(false);
            } else if(in.isKeyPressed(Input.KEY_RIGHT) || in.isKeyPressed(Input.KEY_D) || increaseX.isPressed()) {
                if(xSize < maxXSize) {
                    ++xSize;
                }
                increaseX.setPressed(false);
            }
            if(in.isKeyPressed(Input.KEY_W) || in.isKeyPressed(Input.KEY_UP) || increaseY.isPressed()) {
                if(ySize < maxYSize) {
                    ++ySize;
                }
                increaseY.setPressed(false);
            } else if(in.isKeyPressed(Input.KEY_S) || in.isKeyPressed(Input.KEY_DOWN) || decreaseY.isPressed()) {
                if(ySize > minYSize) {
                    --ySize;
                }
                decreaseY.setPressed(false);
            }
            if(start.isPressed()) {
                start.setPressed(false);
                increaseX.setEnabled(false);
                decreaseX.setEnabled(false);
                increaseY.setEnabled(false);
                decreaseY.setEnabled(false);
                start.setEnabled(false);
                begun = true;
                bombs = new boolean[ySize][xSize];
                int bombs = generator.nextInt(this.bombs.length) + this.bombs[0].length + generator.nextInt((this.bombs.length + this.bombs[0].length)*7);
                while(bombs > 0) {
                    for (int a = 0; a < this.bombs.length; a++) {
                        for (int b = 0; b < this.bombs[0].length; b++) {
                            if (bombs == 0) {
                                break;
                            }
                            if(generator.nextInt(bombs) <= 2) {
                                this.bombs[a][b] = true;
                                --bombs;
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void renderG(GameContainer gc, StateBasedGame game, Graphics g) throws SlickException {
        background.draw(0,0);
        if(!begun && bombs == null) {
            increaseX.render(gc, g);
            decreaseX.render(gc, g);
            increaseY.render(gc, g);
            decreaseY.render(gc, g);
            start.render(gc, g);
            g.drawString("Breite anpassen:", 370, 310);
            g.drawString("Höhe anpassen:", 370, 410);
            g.drawString(""+xSize, 645, 320);
            g.drawString(""+ySize, 645, 420);
        } else {
            for(int a = 0; a < bombs.length; a++) {
                g.drawString("|", 180, 10+a*20);
                g.setColor(Color.white);
                if(a == bombs.length - 1) {
                    g.setColor(Color.orange);
                }
                for(int b = 0; b < bombs[0].length; b++) {
                    fieldID[0].draw(200+b*20, 10+a*20);
                    if(bombs[a][b]) {
                        g.drawString("X", 200+b*20, 10+a*20);
                    }
                }
            }
        }
    }

    @Override
    public void resetMinigame() {

    }

    @Override
    public void renderHelpScreenG(Graphics g) {

    }
}
