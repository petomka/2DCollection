package minigames.snake2d;

import gui.LiveText;
import org.newdawn.slick.*;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.state.StateBasedGame;
import states.MiniGameState;
import util.Point4;
import util.Ticker;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Benedikt on 22.05.2017.
 */
public class Snake2DMain extends MiniGameState implements Ticker.Tickable {

    public final static int ID = 2;

    private boolean[][] occupiedField = new boolean[30][30];

    private int lives = 2;

    private int length = 3;

    private int score = 0;

    private LiveText liveText = new LiveText("", Color.red, 0, 0, 1, 4, true);

    private Ticker ticker;
    private int interval = 12; // alle wie viel ticks die schlange sich bewegt

    private Point4 position = new Point4(15, 15);
    //Dimension Z = Richtung, 0: Nach Unten, 1: Nach Links, 2: Nach Oben, 3: Nach Rechts

    private ArrayList<Point4> snake = new ArrayList<>();

    private boolean isPowerup = false, isPoisonup = false;

    private boolean begun = false;
    private boolean gameOver = false;

    private Image background;

    private Image snakeTileHead, snakeTileYellow, snakeTileOrange, snakeTileTail, powerupTile, lifeupTile, poisonTile;

    Point powerup = new Point(0, 0), poisonup = new Point(0, 0);

    //x285 y95 erster punkt des gitters
    private final int xOffset = 285;
    private final int yOffset = 95;

    public Snake2DMain() throws SlickException {
        super(ID);
    }

    /**
     * 'Moves' the Snake.
     */

    public void onTick() {
        Point4 head = snake.get(snake.size() - 1);
        int dx, dy;
        dx = head.z == 1 ? -1 : (head.z == 3 ? 1 : 0);
        dy = head.z == 0 ? 1 : (head.z == 2 ? -1 : 0);
        Point4 append = new Point4(head.x + dx, head.y + dy, head.z, length);
        if (append.x == 30 || append.x < 0 || append.y == 30 || append.y < 0) {
            loseLife();
            return;
        }
        if(occupiedField[append.x][append.y]) {
            loseLife();
            return;
        }
        for (Point4 p : snake) {
            p.a -= 1;
        }
        snake.add(append);
        occupiedField[append.x][append.y] = true;
        if (append.x == powerup.x && append.y == powerup.y && isPowerup) {
            ++score;
            ++length;
            for (Point4 p : snake) {
                ++p.a;
            }
            isPowerup = false;
            if(length%4 == 0 && interval > 2) {
                --interval;
                ticker.setTickInterval(interval);
            }
            if(score > 0 && (score)%50 == 0) {
                ++lives;
                liveText = new LiveText("Bonusleben!", Color.cyan, xOffset+powerup.x*24-20, yOffset+powerup.y*24, 2, 40, true);
            }
        }
        if(append.x == poisonup.x && append.y == poisonup.y && isPoisonup) {
            liveText = new LiveText("Vergiftet!!", new Color(0xff44ff), xOffset+poisonup.x*24-20, yOffset+poisonup.y*24, 2, 40, true);
            isPoisonup = false;
            poisoned();
        }
        Iterator<Point4> it = snake.iterator();
        ArrayList<Point4> rem = new ArrayList<>();
        while (it.hasNext()) {
            Point4 c = it.next();
            if (c.a <= 0) {
                occupiedField[c.x][c.y] = false;
                rem.add(c);
            }
        }
        snake.removeAll(rem);
        spawnPowerUp();
    }

    @Override
    public void initG(GameContainer gc, StateBasedGame game) throws SlickException {
        isPowerup = false;

        background = new Image("img/snake2d/snake2dBackgroundUI.png");

        snakeTileHead = new Image("img/snake2d/snakeTileHead.png");
        snakeTileOrange = new Image("img/snake2d/snakeTileOrange.png");
        snakeTileYellow = new Image("img/snake2d/snakeTileYellow.png");
        snakeTileTail = new Image("img/snake2d/snakeTileTail.png");
        lifeupTile = new Image("img/snake2d/lifePowerup.png");

        powerupTile = new Image("img/snake2d/applePowerup.png");
        poisonTile = new Image("img/snake2d/poisonPowerup.png");

        ticker = new Ticker(this);
        ticker.setTickInterval(interval);

    }

    @Override
    public void updateG(GameContainer gc, StateBasedGame game, int i) throws SlickException {
        //in = gc.getInput();
        liveText.update();
        if (begun && in.isKeyPressed(Input.KEY_SPACE)) {
            paused = !paused;
        }
        if (!begun) {
            if (in.isKeyDown(Input.KEY_SPACE)) {
                begun = true;
                paused = true;
                if(gameOver) {
                    resetMinigame();
                    gameOver = false;
                }
                for (int a = 0; a < length + 1; a++) {
                    snake.add(new Point4(position.x, position.y, 0, a));
                }
            }
            return;
        }
        if (paused || gameOver) {
            return;
        }
        Point4 head = snake.get(snake.size() - 1);
        Point4 control = snake.get(snake.size() - 2);
        if (in.isKeyDown(Input.KEY_W) || in.isKeyDown(Input.KEY_UP)) {
            if (control.z == 1 || control.z == 3) {
                head.z = 2;
            }
        } else if (in.isKeyDown(Input.KEY_A) || in.isKeyDown(Input.KEY_LEFT)) {
            if (control.z == 0 || control.z == 2) {
                head.z = 1;
            }
        } else if (in.isKeyDown(Input.KEY_S) || in.isKeyDown(Input.KEY_DOWN)) {
            if (control.z == 1 || control.z == 3) {
                head.z = 0;
            }
        } else if (in.isKeyDown(Input.KEY_D) || in.isKeyDown(Input.KEY_RIGHT)) {
            if (control.z == 0 || control.z == 2) {
                head.z = 3;
            }
        }
        ticker.tick();
    }

    @Override
    public void renderG(GameContainer gc, StateBasedGame game, Graphics g) throws SlickException {
        background.draw();
        for (int i = 0; i < snake.size(); i++) {
            Point4 sn = snake.get(i);
            int x = xOffset + sn.x * 24, y = yOffset + sn.y * 24;
            if (sn.a == length && sn.a != 1) {
                snakeTileHead.rotate(90 * sn.z);
                snakeTileHead.draw(x, y);
                snakeTileHead.rotate(-90 * sn.z);
            } else if (sn.a == 1) {
                snakeTileTail.rotate(90 * sn.z);
                snakeTileTail.draw(x, y);
                snakeTileTail.rotate(-90 * sn.z);
            } else if (sn.a % 4 == 0) {
                snakeTileYellow.draw(x, y);
            } else {
                snakeTileOrange.draw(x, y);
            }
        }
        if (isPowerup) {
            if(score > 0 && (score+1)%50 == 0) {
                lifeupTile.draw(xOffset + powerup.x * 24, yOffset + powerup.y * 24);
            } else {
                powerupTile.draw(xOffset + powerup.x * 24, yOffset + powerup.y * 24);
            }
        }
        if(isPoisonup) {
            poisonTile.draw(xOffset + poisonup.x * 24, yOffset + poisonup.y * 24);
        }
        powerupTile.draw(100, 220);
        g.setColor(Color.white);
        int x = score;
        StringBuilder str = new StringBuilder();
        for(int s = 0; s < 3-String.valueOf(x).length(); s++) {
            str.append("0");
        }
        str.append(x);
        g.drawString("x "+str.toString(), 130, 223);
        snakeTileHead.draw(1130,220);
        g.drawString("x " + lives, 1165, 220);
        liveText.render(g);
        if (paused && begun && !gameOver) {
            g.setColor(Color.white);
            g.drawString("Pausiert!", 560, 400);
        }
        if (!begun && !gameOver) {
            g.setColor(Color.white);
            g.drawString("Drücke LEER um zu starten!", 530, 400);
        }
        if(gameOver) {
            g.setColor(Color.red);
            g.drawString("Game Over! Drücke LEER, um neuzustarten!", 530, 400);
        }
    }

    private void spawnPowerUp() {
        if (isPowerup) {
            return;
        }
        int x = generator.nextInt(29);
        int y = generator.nextInt(29);
        if (occupiedField[x][y] && !(isPoisonup && x == poisonup.x && y == poisonup.y)) {
            return;
        }
        if(!isPoisonup) {
            int spawnPoison = generator.nextInt(60);
            if(spawnPoison == 47) {
                int x1 = generator.nextInt(29);
                int y1 = generator.nextInt(29);
                if (x1 == x || y1 == y) {
                    return;
                }
                poisonup.x = x1;
                poisonup.y = y1;
                isPoisonup = true;
            }
        }
        isPowerup = true;
        powerup = new Point(x, y);
    }

    private void loseLife() {
        --lives;
        if(lives < 0) {
            gameOver();
            lives = 0;
        }
        begun = false;
        for (Point4 p : snake) {
            p.x = 15;
            p.y = 15;
            p.z = 0;
        }
        for(int a = 0; a < occupiedField.length; a++) {
            for(int b = 0; b < occupiedField[a].length; b++) {
                occupiedField[a][b] = false;
            }
        }
    }

    private void poisoned() {
        --lives;
        if(lives < 0) {
            gameOver();
            lives = 0;
            return;
        }
        length = length/2;
        interval = interval*3;
        ticker.setTickInterval(interval);
        if(length < 3) {
            gameOver();
            return;
        }
        for(int a = 0; a < snake.size(); a++) {
            snake.get(a).a = a-length;
        }
    }

    private void gameOver() {
//        resetMinigame();
        snake = new ArrayList<>();
        gameOver = true;
        begun = false;
//        length = 3;
//        interval = 12;
//        lives = 2;
//        isPowerup = false;
//        score = 0;
//        snake = new ArrayList<>();
//        occupiedField = new boolean[30][30];
//        ticker.setTickInterval(interval);
//        ticker.setTicks(0);
    }

    @Override
    public void resetMinigame() {
        lives = 2;
        length = 3;
        begun = false;
        paused = false;
        gameOver = false;
        position.x = 15;
        position.y = 15;
        position.z = 0;
        isPowerup = false;
        isPoisonup = false;
        score = 0;
        snake = new ArrayList<>();
        interval = 12;
        ticker.setTickInterval(interval);
        ticker.setTicks(0);
        occupiedField = new boolean[30][30];
        liveText = new LiveText("", Color.white, xOffset+powerup.x*24-20, yOffset+powerup.y*24, 2, 40, true);
    }

    @Override
    public void renderHelpScreenG(Graphics g) {
        Color prev = g.getColor();
        g.setColor(Color.white);
        g.drawString("Das Ziel ist es, möglichst viele Äpfel zu essen. Du hast am Anfang 3 Leben, kannst aber im Laufe des Spiels weitere Leben \n" +
                "einsammeln. Du verlierst ein Leben wenn du gegen eine Wand oder in dich selbst fährst. Wenn dein letztes Leben verbraucht\n" +
                "ist, ist dieses Spiel vorbei.", 108, 170);
        powerupTile.draw(180, 280);
        g.setColor(new Color(0x11ff11));
        g.drawString("Wenn die Schlange einen grünen Apfel isst, verlängert sie sich um ein Segment. Dabei wird\n" +
                "die Schlange auch schneller!", 240, 274);

        lifeupTile.draw(180, 350);
        g.setColor(new Color(0xff4444));
        g.drawString("Wenn die Schlange einen roten Apfel isst, verlängert sie sich um ein Segment und bekommt ein\n" +
                "Extraleben. Dabei wird die Schlange auch schneller!", 240, 345);

        poisonTile.draw(180, 420);
        g.setColor(new Color(0xff44ff));
        g.drawString("Wenn die Schlange einen vergifteten Apfel isst, halbiert sie sich und wird langsamer, \n" +
                "aber sie verliert dadurch ein Leben! Isst sie den Apfel ohne ein verbleibendes Extraleben, \n" +
                "is(s)t das Spiel vorbei. Ist die Schlange zu klein, um halbiert zu werden, endet das Spiel\n" +
                "sofort.", 240, 400);

        wasd.draw(200, 530);
        g.setColor(Color.yellow);
        g.drawString("Steuere die Schlange mit WASD oder den Pfeiltasten. \nMit der Leertaste kannst du das Spiel jederzeit pausieren.", 470, 575);

        g.setColor(Color.white);
        g.drawString("Die maximale Geschwindigkeit der Schlange liegt bei 10 Bewegungen pro Sekunde. \n" +
                "Die Startgeschwindigkeit ist 1,66 Bewegungen pro Sekunde. Die Schlange ist am Anfang 3 Segmente lang.\n" +
                "Die Schlange könnte maximal 2^32/2 (=2147483648) Segmente lang werden.", 108, 680);
        g.setColor(prev);
    }

    public Image getBackground() {
        return background;
    }

    public Image getSnakeTileHead() {
        return snakeTileHead;
    }

    public Image getSnakeTileYellow() {
        return snakeTileYellow;
    }

    public Image getSnakeTileOrange() {
        return snakeTileOrange;
    }

    public Image getSnakeTileTail() {
        return snakeTileTail;
    }

    public Image getPowerupTile() {
        return powerupTile;
    }

    public Image getLifeupTile() {
        return lifeupTile;
    }
}
