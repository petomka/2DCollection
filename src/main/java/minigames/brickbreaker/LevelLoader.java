package minigames.brickbreaker;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import lombok.experimental.UtilityClass;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class loads Level objects from a .txt file with a horrible Syntax
 */
@UtilityClass
public class LevelLoader {

	private List<Level> levels = Lists.newArrayList();

	public void loadLevels() {
		Gson gson = new Gson();
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("levelfiles/brickbreakerlevels.json");
		Reader reader = new InputStreamReader(is);
		Level[] levelArray = gson.fromJson(reader, Level[].class);
		levels.addAll(Lists.newArrayList(levelArray));
		levels.forEach(Level::initializeBricks);
	}

	public int getMaxLevels() {
		return levels.size();
	}

	public static Level getLevel(int id) {
		if(id < 0 || id >= levels.size()) {
			throw new RuntimeException("Level id is out of bounds! Was: " + id + ", minimum 0, maximum " + levels.size());
		}
		return levels.get(id).clone();
	}

}
