package minigames.brickbreaker;

/**
 * Bonus class
 * this is just a bonus on top
 */
public class Bonus {
    double x;
    double y;
    int ID;
    double vY;
    public Bonus(int x, int y, double vY, int ID) {
        this.x = x;
        this.y = y;
        this.ID = ID;
        this.vY = vY;
    }
}
