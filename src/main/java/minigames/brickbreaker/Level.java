package minigames.brickbreaker;

import com.google.common.collect.Lists;
import lombok.Data;
import util.CopyUtils;

import java.awt.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.List;

import static minigames.brickbreaker.BrickbreakerMain.paddleSize;

/**
 * A class for a object that represents a level made of bricks with a background
 */
@Data
public class Level {

	private int width;
	private int heigth;

	private int paddleID;
	private int ballID;
	private int backgroundID;

	public Level(int paddleID, int ballID, int backgroundID, Brick[][] bricks) {
		this.paddleID = paddleID;
		this.ballID = ballID;
		this.backgroundID = backgroundID;
		this.bricks = bricks;
		this.heigth = bricks.length;
		this.width = bricks[0].length;
		forEveryIndexedBrick((b, p) -> {
			b.setGridX(p.x);
			b.setGridY(p.y);
		});
	}

	private Brick[][] bricks;

	public int getWidth() {
		return width;
	}

	public int getHeigth() {
		return heigth;
	}

	public Brick getBrick(int gridX, int gridY) {
		if (gridX < 0 || gridY < 0) {
			return null;
		}
		if (gridY >= bricks.length || gridX >= bricks[0].length) {
			return null;
		}
		return bricks[gridY][gridX];
	}

	public int getPaddleID() {
		return paddleID + paddleSize * 7;
	}

	public int getBallID() {
		return ballID;
	}

	public int getBackgroundID() {
		return backgroundID;
	}

	public Level clone() {
		Level clone = new Level(paddleID, ballID, backgroundID, bricks);
		CopyUtils<Brick> copyUtils = new CopyUtils<>(Brick.class);
		clone.bricks = copyUtils.deep2DArrayCopyOf(bricks);
		return clone;
	}

	public boolean isFinished() {
		AtomicBoolean finished = new AtomicBoolean(true);
		forEveryBrick((b) -> {
			if (b.isRelevant()) {
				finished.set(false);
			}
		});
		return finished.get();
	}

	public int visibleBricksLeft() {
		AtomicInteger bricks = new AtomicInteger(0);
		forEveryBrick(b -> {
			if (b.isRelevant() && !b.isInvisible()) {
				bricks.incrementAndGet();
			}
		});
		return bricks.get();
	}

	public void unveilHiddenBricks() {
		forEveryBrick(b -> b.setInvisible(false));
	}

	public void initializeBricks() {
		forEveryBrick(Brick::init);
	}

	public void forEveryBrick(Consumer<Brick> consumer) {
		for (Brick[] brickRows : bricks) {
			for (Brick brick : brickRows) {
				if (brick != null) {
					consumer.accept(brick);
				}
			}
		}
	}

	public void forEveryIndexedBrick(BiConsumer<Brick, Point> consumer) {
		for(int y = 0; y < bricks.length; y++) {
			for(int x = 0; x < bricks[y].length; x++) {
				if(bricks[y][x] == null) {
					continue;
				}
				consumer.accept(bricks[y][x], new Point(x, y));
			}
		}
	}

	public List<Brick> getAdjacentBricks(Brick center) {
		List<Brick> adjacentBricks = Lists.newArrayList();
		for(int x = -1; x < 1; x++) {
			for(int y = -1; y < 1; y++) {
				if(x == 0 && y == 0) {
					continue;
				}
				Brick adjacentBrick = getBrick(center.getGridX() + x, center.getGridY() + y);
				if(adjacentBrick == null) {
					continue;
				}
				adjacentBricks.add(adjacentBrick);
			}
		}
		return adjacentBricks;
	}

}
