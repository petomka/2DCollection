package minigames.brickbreaker;

import lombok.Data;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import java.io.Serializable;

@Data
public class Brick implements Serializable {

	private static Image[] brickImages;
	private static Image[] crackImages;

	static {
		try {
			brickImages = new Image[29];
			for (int i = 0; i < brickImages.length; i++) {
				brickImages[i] = new Image("img/brickbreaker/bricks/brick" + (String.valueOf((i + 1)).length() == 1 ? "0" : "") + (i + 1) + ".png");
			}
			crackImages = new Image[5];
			for (int i = 0; i < crackImages.length; i++) {
				crackImages[i] = new Image("img/brickbreaker/cracks/crack" + (String.valueOf((i + 1)).length() == 1 ? "0" : "") + (i + 1) + ".png");
			}

		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

	private int b; //Brick id -> Image
	private int h; //health -> how often it needs to be hit
	private boolean i; //invisible -> needs to be hit once in order to be visible

	private int initialHealth;
	private boolean transparent;

	private int gridX = 0;
	private int gridY = 0;

	public void init() {
		initialHealth = h;
		transparent = i;
	}

	public void draw(int xOffset, int yOffset) {
		if (isInvisible() || !isRelevant()) {
			return;
		}
		if(isTransparent()) {
			brickImages[b].setAlpha(.5f);
			brickImages[b].draw(xOffset + this.gridX * 32, yOffset + this.gridY * 16);
			brickImages[b].setAlpha(1f);
		} else {
			brickImages[b].draw(xOffset + this.gridX * 32, yOffset + this.gridY * 16);
		}
		if(h < initialHealth && initialHealth > 1 && h > 0) {
			int dmg = initialHealth-h;
			if(dmg <= 0) {
				return;
			}
			if(dmg >= crackImages.length) {
				dmg = crackImages.length;
			}
			crackImages[dmg-1].draw(xOffset + 32 * this.gridX, yOffset + this.gridY * 16);
		}
	}

	public boolean isRelevant() {
		return b > 0 && !isDestroyed();
	}

	public boolean isDestroyed() {
		return h <= 0;
	}

	public boolean isInvisible() {
		return i;
	}

	public void setInvisible(boolean invisible) {
		this.i = invisible;
	}

	public int getHealth() {
		return h;
	}

	public void setHealth(int health) {
		this.h = health;
	}

	public void damage() {
		if(isInvisible()) {
			i = false;
			return;
		}
		this.h -= 1;
	}

}
