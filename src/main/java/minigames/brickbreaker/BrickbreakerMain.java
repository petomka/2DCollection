package minigames.brickbreaker;

import lombok.Getter;
import org.newdawn.slick.*;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.StateBasedGame;
import states.MiniGameState;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Please don't even try to understand the "math" that happens here
 * trial and error
 * and it still doesnt work properly after so many hours
 */
@Getter
public class BrickbreakerMain extends MiniGameState {

	public static final int ID = 7;

	@Getter
	private static BrickbreakerMain instance;

	public BrickbreakerMain() {
		super(ID);
		instance = this;
		LevelLoader.loadLevels();
	}

	private Point paddlePosition;

	private ArrayList<Ball> balls = new ArrayList<>();
	private ArrayList<Bonus> bonuses = new ArrayList<>();

	private int paddleTargetX = 1300 / 2;

	private static final int PADDLE_MAX_VELOCITY = 20;

	private Image[] heartID;

	private Image[] paddleID;

	private Image[] backgroundID;

	private Image[] bonusID;

	private int maxLives = 3;
	private int lives = 3;

	private boolean stickyPaddle = false;

	private boolean begun = false;

	private boolean preBegun = false;

	private boolean gameOver = false;

	private boolean cometBall = false;

	private boolean explosiveBall = false;

	private double accFactor = 7;

	private int levelNumber = 0;

	private int maxLevel = LevelLoader.getMaxLevels();

	private int score = 0;

	private int paddleVX = 0;

	static int paddleSize = 0;

	private static final int MAX_PADDLE_SIZE = 3;
	private static final int SCORE_UNVEIL_BRICK = 5;
	private static final int SCORE_DAMAGE_BRICK = 3;
	private static final int SCORE_DESTROY_BRICK = 9;
	private static final int SPAWN_BONUS_CHANCE = 1000; // 8,5% -> of thousand

	private Level level = null;

	private int xOffset, yOffset = 75;

	private Point closest_point(Rectangle rect, Circle circle) {
		return new Point((int) Math.max(rect.getMinX(), Math.min(rect.getMaxX(), circle.getCenterX())), (int) Math.max(rect.getMinY(), Math.min(rect.getMaxY(), circle.getCenterY())));
	}

	boolean hit_test(Rectangle rect, Circle circle, int radius, Rectangle box) {
		if (box != null && !rect.intersects(box)) {
			return false;
		}
		Point p = closest_point(rect, circle);
		double vecX = p.x - circle.getCenterX();
		double vecY = p.y - circle.getCenterY();
		double length = Math.sqrt(Math.pow(vecX, 2) + Math.pow(vecY, 2));
		return length < radius;
	}

	@Override
	public void initG(GameContainer gc, StateBasedGame game) throws SlickException {

		paddleID = new Image[28];
		for (int i = 0; i < paddleID.length; i++) {
			paddleID[i] = new Image("img/brickbreaker/paddles/paddle" + (String.valueOf((i + 1)).length() == 1 ? "0" : "") + (i + 1) + ".png");
		}

		backgroundID = new Image[18];
		for (int i = 0; i < backgroundID.length; i++) {
			backgroundID[i] = new Image("img/brickbreaker/backgrounds/background" + (String.valueOf((i + 1)).length() == 1 ? "0" : "") + (i + 1) + ".png");
		}

		bonusID = new Image[13];
		for (int i = 0; i < bonusID.length; i++) {
			bonusID[i] = new Image("img/brickbreaker/bonuses/bonus" + (String.valueOf((i + 1)).length() == 1 ? "0" : "") + (i + 1) + ".png", false, Image.FILTER_NEAREST);
		}

		heartID = new Image[]{new Image("img/brickbreaker/heartEmpty.png", false, Image.FILTER_NEAREST), new Image("img/brickbreaker/heartFull.png", false, Image.FILTER_NEAREST)};

		maxLevel = LevelLoader.getMaxLevels();
		level = LevelLoader.getLevel(levelNumber);
		paddlePosition = new Point(1300 / 2, 864 - 80);
		if (balls.isEmpty()) {
			balls.add(new Ball(1300 / 2, 864 - 80 - (paddleID[level.getPaddleID()].getHeight() / 2 +
					Ball.getBallImages()[level.getBallID()].getHeight() / 2), 0, 0, level.getBallID(), 0));
		}

		xOffset = (1300 / 2) - level.getWidth() * 16;

		BBFont.init();
		Trail.init();
	}

	public void calcPaddleVX() {
		paddleVX = paddleTargetX - paddlePosition.x;
		if (Math.abs(paddleVX) > PADDLE_MAX_VELOCITY) {
			if (paddleVX < 0) {
				paddleVX = -PADDLE_MAX_VELOCITY;
			} else {
				paddleVX = PADDLE_MAX_VELOCITY;
			}
		}
		if (paddleVX < 0 && paddlePosition.x - paddleVX < paddleID[level.getPaddleID()].getWidth()) {
			paddleVX = 0;
		} else if (paddleVX > 0 && paddlePosition.x + paddleVX > 1300 - paddleID[level.getPaddleID()].getWidth() / 2) {
			paddleVX = 0;
		}
	}

	@Override
	public void updateG(GameContainer gc, StateBasedGame game, int i) throws SlickException {
		if (in.isKeyPressed(Input.KEY_SPACE)) {
			if (begun && preBegun) {
				paused = !paused;
			}
		}
		if (explosiveBall || cometBall) {
			trail.updateTrail(explosiveBall, balls);
		}
		if (gameOver) {
			return;
		}
		calcPaddleVX();
		balls.forEach(Ball::move);
		if (!paused) {
			paddlePosition.x += paddleVX;
		}
		if (!begun || paused) {
			return;
		}
		List<Ball> lostBalls = balls.stream().filter(Ball::isOutOfBounds).collect(Collectors.toList());
		balls.removeAll(lostBalls);
		if (balls.isEmpty()) {
			loseLife();
			return;
		}
		bounceBallFromPaddle(false);
		ArrayList<Bonus> bonusesToRemove = new ArrayList<>();
		try {
			outer:
			for (Bonus bonus : bonuses) {
				for (double a = 0; a < bonus.vY; a += 0.1) {
					bonus.y += 0.1;
					if (bonus.x >= paddlePosition.x - paddleID[level.getPaddleID()].getWidth() / 2 - bonusID[bonus.ID].getWidth() / 2 &&
							bonus.x <= paddlePosition.x + paddleID[level.getPaddleID()].getWidth() / 2 + bonusID[bonus.ID].getWidth() / 2 &&
							bonus.y >= paddlePosition.y - paddleID[level.getPaddleID()].getHeight() / 2 - bonusID[bonus.ID].getHeight() / 2 &&
							bonus.y <= paddlePosition.y + paddleID[level.getPaddleID()].getHeight() / 2 + bonusID[bonus.ID].getHeight() / 2) {
						giveBonus(bonus.ID);
						bonusesToRemove.add(bonus);
						continue outer;
					}
				}
			}
		} catch (Exception e) {
			//mute
		}
		bonuses.removeAll(bonusesToRemove);
		nextLevel();
	}

	public boolean collideWithBrick(double x, double y) {
		if (x - xOffset < 0 || y - yOffset < 0) {
			return false;
		}
		int column = (int) ((x - xOffset) / 32);
		int row = (int) ((y - yOffset) / 16);
		if (column < 0 || column >= level.getWidth()) {
			return false;
		}
		if (row < 0 || row >= level.getHeigth()) {
			return false;
		}
		Brick brick = level.getBrick(column, row);
		if (brick == null) {
			return false;
		}
		if (brick.isDestroyed()) {
			return false;
		}
		if (brick.isInvisible()) {
			brick.setInvisible(false);
			score += SCORE_UNVEIL_BRICK;
			return true;
		}
		if (cometBall) {
			score += SCORE_DAMAGE_BRICK * (brick.getHealth() - 1);
			score += SCORE_DESTROY_BRICK;
			brick.setHealth(0);
			spawnMaybeBonus((int) x, (int) y);
			return false;
		}
		brick.damage();
		score += SCORE_DAMAGE_BRICK;
		if (brick.isDestroyed()) {
			score += SCORE_DESTROY_BRICK;
			spawnMaybeBonus((int) x, (int) y);
		}
		if (explosiveBall) {
			List<Brick> adjacentBricks = level.getAdjacentBricks(brick);
			adjacentBricks.stream().filter(Brick::isInvisible).forEach(b -> score += SCORE_UNVEIL_BRICK);
			adjacentBricks.stream().filter(b -> !b.isInvisible()).forEach(b -> score += SCORE_DAMAGE_BRICK);
			adjacentBricks.forEach(Brick::damage);
			adjacentBricks.stream().filter(Brick::isDestroyed).forEach(b -> score += SCORE_DESTROY_BRICK);
		}
		return true;
	}

	private void bounceBallFromPaddle(boolean force) {
		if (stickyPaddle && !force) {
			return;
		}
		for (Ball b : balls) {
			b.stuckOnPaddle = false;
			Image[] ballImages = Ball.getBallImages();
			if ((b.y + ballImages[b.id].getHeight() / 2f + paddleID[level.getPaddleID()].getHeight() / 2f >= paddlePosition.y)
					&& b.y < paddlePosition.y
					&& b.x + ballImages[b.id].getWidth() / 2 >= paddlePosition.x - paddleID[level.getPaddleID()].getWidth() / 2
					&& b.x - ballImages[b.id].getWidth() / 2 <= paddlePosition.x + paddleID[level.getPaddleID()].getWidth() / 2) {
				//for whatever reason this works
				double div = (paddleID[level.getPaddleID()].getWidth() / 2f) / 0.56;
				double vX = Math.cos(1 - (((paddleID[level.getPaddleID()].getWidth() / 2f + (b.x - paddlePosition.x)) - paddleID[level.getPaddleID()].getWidth()) / div));
				double vY = -Math.sin(1 - (((paddleID[level.getPaddleID()].getWidth() / 2f + (b.x - paddlePosition.x)) - paddleID[level.getPaddleID()].getWidth()) / div));
				b.y = (paddlePosition.y - paddleID[level.getPaddleID()].getHeight() / 2 - ballImages[b.id].getHeight() / 2) - 1;
				b.vX = vX;
				b.vY = vY;
			}
		}
	}

	private void nextLevel() {
		nextLevel(false);
	}

	private void nextLevel(boolean force) {
		if (level.visibleBricksLeft() == 0) {
			level.unveilHiddenBricks();
		}
		if (!level.isFinished() && !force) {
			return;
		}
		++levelNumber;
		if (levelNumber >= maxLevel) {
			levelNumber = 0;
		}
		level = LevelLoader.getLevel(levelNumber);
		xOffset = (1300 / 2) - level.getWidth() * 16;
		cometBall = false;
		explosiveBall = false;
		trail = new Trail();
		stickyPaddle = false;
		balls.clear();
		balls.add(new Ball(1300 / 2, 864 - 80 - (paddleID[level.getPaddleID()].getHeight() / 2 +
				Ball.getBallImages()[level.getBallID()].getHeight() / 2) - 80, 0, 0.1, level.getBallID(), 0));
	}

	private void previousLevel() {
		if (levelNumber == 0) {
			return;
		}
		--levelNumber;
		level = LevelLoader.getLevel(levelNumber);
		xOffset = (1300 / 2) - level.getWidth() * 16;
		balls.clear();
		balls.add(new Ball(1300 / 2, 864 - 80 - (paddleID[level.getPaddleID()].getHeight() / 2 +
				Ball.getBallImages()[level.getBallID()].getHeight() / 2) - 80, 0, 0.1, level.getBallID(), 0));
	}

	private void loseLife() {
		--lives;
		if (lives <= 0) {
			gameOver = true;
			return;
		}
		cometBall = false;
		explosiveBall = false;
		stickyPaddle = false;
		accFactor = 7;
		balls.clear();
		bonuses.clear();
		trail = new Trail();
		balls.add(new Ball(paddlePosition.x, 864 - 80 - (paddleID[level.getPaddleID()].getHeight() / 2 +
				Ball.getBallImages()[level.getBallID()].getHeight() / 2), 0, 0, level.getBallID(), 0));
		begun = false;
	}

	private void spawnMaybeBonus(int x, int y) {
		if (generator.nextInt(1000) <= SPAWN_BONUS_CHANCE) {
			int ID = generator.nextInt(bonusID.length);
			Bonus bonus = new Bonus(x - bonusID[ID].getWidth() / 2, y - bonusID[ID].getHeight() / 2, generator.nextDouble() * 2.5 + 2.0, ID);
			bonuses.add(bonus);
		}
	}

	private void giveBonus(int ID) {
		switch (ID) {
			case 0:
				loseLife();
				break;
			case 1:
				previousLevel();
				break;
			case 2:
				if (accFactor < 15) {
					accFactor += 2;
				}
				break;
			case 3:
				if (accFactor > 5) {
					accFactor -= 2;
				}
				break;
			case 4:
				ID = generator.nextInt(7);
				balls.add(new Ball(1300 / 2, 864 - 80 - (paddleID[level.getPaddleID()].getHeight() / 2 +
						Ball.getBallImages()[ID].getHeight() / 2) - 80, 0, 0.1, ID, 0));
				break;
			case 5:
				cometBall = true;
				explosiveBall = false;
				break;
			case 6:
				cometBall = false;
				explosiveBall = true;
				break;
			case 7:
				nextLevel(true);
				break;
			case 8:
				growPaddle();
				break;
			case 9:
				shrinkPaddle();
				break;
			case 10:
				stickyPaddle = true;
				break;
			case 11:
				for (Ball b : balls) {
					b.setSize(b.getSize() - 1);
				}
				break;
			case 12:
				for (Ball b : balls) {
					b.setSize(b.getSize() + 1);
				}
			default:
				break;
		}
	}

	private void growPaddle() {
		if (paddleSize < MAX_PADDLE_SIZE) {
			++paddleSize;
		}
	}

	private void shrinkPaddle() {
		if (paddleSize > 0) {
			--paddleSize;
		}
	}

	@Override
	public void mouseMoved(int oldx, int oldy, int newx, int newy) {
		paddleTargetX = newx;
	}

	@Override
	public void mouseClicked(int button, int x, int y, int clickCount) {
		if (gameOver) {
			resetMinigame();
			return;
		}
		if (!preBegun) {
			preBegun = true;
			return;
		}
		if (!begun) {
			begun = true;
		}
		bounceBallFromPaddle(true);
	}

	Trail trail = new Trail();

	@Override
	public void renderG(GameContainer gc, StateBasedGame game, Graphics g) throws SlickException {
		backgroundID[level.getBackgroundID()].draw(0, 0);
		level.forEveryBrick(b -> b.draw(xOffset, yOffset));
		if (!gameOver && !paused) {
			paddleID[level.getPaddleID()].draw(paddlePosition.x - (paddleID[level.getPaddleID()].getWidth() / 2f), paddlePosition.y - paddleID[level.getPaddleID()].getHeight() / 2f);
		} else if (!paused) {
			g.drawString("Drücke LMT um neuzustarten!", 1300 / 2 - 150, 864 / 2 - 12);
		}
		if (explosiveBall || cometBall) {
			trail.drawTrail(explosiveBall);
		}
		balls.forEach(Ball::draw);
		for (int i = 0; i < maxLives; i++) {
			if (i < lives) {
				heartID[1].draw(1300 - heartID[1].getWidth() * 3 - (maxLives * i) * (heartID[1].getWidth() + 1) - 5, 5, 3);
			} else {
				heartID[0].draw(1300 - heartID[0].getWidth() * 3 - (maxLives * i) * (heartID[0].getWidth() + 1) - 5, 5, 3);
			}
		}
		for (Bonus bonus : bonuses) {
			bonusID[bonus.ID].draw((float) bonus.x - bonusID[bonus.ID].getWidth() / 2f, (float) bonus.y - bonusID[bonus.ID].getHeight() / 2f);
		}
		StringBuilder scoreString = new StringBuilder();
		for (int i = 8 - String.valueOf(score).length(); i > 0; i--) {
			scoreString.append("0");
		}
		scoreString.append(score);
		BBFont.draw("score: " + scoreString.toString(), 570, 5);
		if (paused) {
			BBFont.draw("Pausiert!", 605, 860 / 2);
		}
	}

	@Override
	public void resetMinigame() {
		level = LevelLoader.getLevel(0);
		levelNumber = 0;
		paddlePosition = new Point(1300 / 2, 864 - 80);
		balls.clear();
		balls.add(new Ball(paddlePosition.x, 864 - 80 - (paddleID[level.getPaddleID()].getHeight() / 2 +
				Ball.getBallImages()[level.getBallID()].getHeight() / 2), 0, 0, level.getBallID(), 0));
		begun = false;
		accFactor = 7;
		preBegun = false;
		gameOver = false;
		cometBall = false;
		explosiveBall = false;
		stickyPaddle = false;
		paused = false;
		score = 0;
		xOffset = (1300 / 2) - level.getWidth() * 16;

		bonuses.clear();
		paddleSize = 0;

		trail = new Trail();

		maxLives = 3;
		lives = 3;
	}

	@Override
	public void renderHelpScreenG(Graphics g) {

	}
}
