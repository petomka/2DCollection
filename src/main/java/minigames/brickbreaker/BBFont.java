package minigames.brickbreaker;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

/**
 * Custom Pixel Font
 * could have been done easier
 * ¯\_(ツ)_/¯
 */
public class BBFont {

    private static Image[] chars;

    private static  Image[] digits;

    private static Image[] symbols;

    public static void init() throws SlickException {
        chars = new Image[26];
        for(int i = 0; i < chars.length; i++) {
            chars[i] = new Image("img/brickbreaker/font/char" + (String.valueOf((i + 1)).length() == 1 ? "0" : "") + (i + 1) + ".png", false, Image.FILTER_NEAREST);
        }

        digits = new Image[10];
        for(int i = 0; i < digits.length; i++) {
            digits[i] = new Image("img/brickbreaker/font/digit" +  i + ".png", false, Image.FILTER_NEAREST);
        }

        symbols = new Image[3];
        for(int i = 0; i < symbols.length; i++) {
            symbols[i] = new Image("img/brickbreaker/font/sym" + (i + 1) + ".png", false, Image.FILTER_NEAREST);
        }
    }

    public static void draw(String text, int x, int y) {
        text = text.toUpperCase();
        for(int index = 0; index < text.length(); index++) {
            int charInt = (int) text.charAt(index);
            if(charInt >= 65 && charInt <= 90) {
                chars[charInt-65].draw(x+((chars[charInt-65].getWidth()+1)*index*2), y, 2);
            } else if(charInt >= 48 && charInt <= 57) {
                digits[charInt-48].draw(x+((digits[charInt-48].getWidth()+1)*index*2), y, 2);
            } else if(charInt == 58) {
                symbols[0].draw(x+((symbols[0].getWidth()+1)*index*2), y, 2);
            } else if(charInt == 33) {
                symbols[1].draw(x+((symbols[0].getWidth()+1)*index*2), y, 2);
            } else if(charInt == 39) {
                symbols[2].draw(x+((symbols[0].getWidth()+1)*index*2), y, 2);
            }
        }
    }

}
