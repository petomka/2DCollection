package minigames.brickbreaker;

import lombok.Getter;
import main.Main;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import java.awt.*;

/**
 * Ball class for various properties
 */
public class Ball {

	@Getter
	private static Image[] ballImages;

	static {
		try {
			ballImages = new Image[21];
			for (int i = 0; i < ballImages.length; i++) {
				ballImages[i] = new Image("img/brickbreaker/balls/ball" + (String.valueOf((i + 1)).length() == 1 ? "0" : "") + (i + 1) + ".png");
			}
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

	double vX;
	double vY;
	double x;
	double y;
	int id;
	private int size;
	int powerup; //unused
	boolean stuckOnPaddle = false;

	public Ball(int x, int y, double vX, double vY, int id, int size) {
		this(x, y, vX, vY, id, size, 0);
	}

	public Ball(int x, int y, double vX, double vY, int id, int size, int powerup) {
		this.x = x;
		this.y = y;
		this.vX = vX;
		this.vY = vY;
		this.id = id + (size * 7);
		this.size = size;
		this.powerup = powerup;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		if (size > 2) {
			size = 2;
		} else if (size < 0) {
			size = 0;
		}
		this.id = this.id - (this.size * 7);
		this.size = size;
		this.id = this.id + (size * 7);
	}

	public void draw() {
		ballImages[id].draw((int) x - ballImages[id].getWidth() / 2f, (int) y - ballImages[id].getHeight() / 2f);
	}

	public void move() {
		//wenn ball auf dem paddle und sticky oder noch nicht angefangen
		boolean stickyPaddle = BrickbreakerMain.getInstance().isStickyPaddle();
		boolean begun = BrickbreakerMain.getInstance().isBegun();
		boolean paused = BrickbreakerMain.getInstance().isPaused();
		Image[] paddleID = BrickbreakerMain.getInstance().getPaddleID();
		Level level = BrickbreakerMain.getInstance().getLevel();
		Point paddlePosition = BrickbreakerMain.getInstance().getPaddlePosition();
		float paddleVX = BrickbreakerMain.getInstance().getPaddleVX();
		if ((stuckOnPaddle || !begun) && !paused) {
			this.x += paddleVX;
			if (checkPaddleCollision() || stickyPaddle) {
				this.stuckOnPaddle = true;
				this.y = (paddlePosition.y - paddleID[level.getPaddleID()].getHeight() / 2 - ballImages[this.id].getHeight() / 2);
				this.vX = 0;
				this.vY = 0;
			}
		}
		if (paused || !begun) {
			return;
		}
		for (int iteration = 0; iteration < BrickbreakerMain.getInstance().getAccFactor(); iteration++) {
			if (this.x <= 0) {
				vX = -vX;
			}
			if (this.x + ballImages[this.id].getWidth() >= Main.getWindowWidth()) {
				vX = -vX;
			}
			if (this.y + ballImages[this.id].getHeight() <= 0) {
				vY = -vY;
			}
			if (checkPaddleCollision()) {
				if (BrickbreakerMain.getInstance().isStickyPaddle()) {
					vY = 0;
					this.stuckOnPaddle = true;
				} else {
					double div = (paddleID[level.getPaddleID()].getWidth() / 2f) / 0.56;
					double vX = Math.cos(1 - (((paddleID[level.getPaddleID()].getWidth() / 2f + (x - paddlePosition.x)) - paddleID[level.getPaddleID()].getWidth()) / div));
					double vY = -Math.sin(1 - (((paddleID[level.getPaddleID()].getWidth() / 2f + (x - paddlePosition.x)) - paddleID[level.getPaddleID()].getWidth()) / div));
					this.y = (paddlePosition.y - paddleID[level.getPaddleID()].getHeight() / 2 - ballImages[id].getHeight() / 2) - 1;
					this.vX = vX;
					this.vY = vY;
				}
			}
			this.x += vX;
			if (checkBrickCollisions()) {
				this.vX = -vX;
				this.x += 2 * vX;
			}
			this.y += vY;
			if (checkBrickCollisions()) {
				this.vY = -vY;
				this.y += 2 * vY;
			}
		}
	}

	public void releaseFromPaddle() {
		this.stuckOnPaddle = false;
	}

	private boolean checkBrickCollisions() {
		return BrickbreakerMain.getInstance().collideWithBrick(x, y) ||
				BrickbreakerMain.getInstance().collideWithBrick(x, y + ballImages[id].getHeight()) ||
				BrickbreakerMain.getInstance().collideWithBrick(x + ballImages[id].getWidth(), y) ||
				BrickbreakerMain.getInstance().collideWithBrick(x + ballImages[id].getWidth(), y + ballImages[id].getHeight());
	}

	private boolean checkPaddleCollision() {
		Point paddlePos = BrickbreakerMain.getInstance().getPaddlePosition();
		int paddleWidth = BrickbreakerMain.getInstance().getPaddleID()[BrickbreakerMain.getInstance().getLevel().getPaddleID()]
				.getWidth();
		int paddleHeight = BrickbreakerMain.getInstance().getPaddleID()[BrickbreakerMain.getInstance().getLevel().getPaddleID()]
				.getHeight();
		if (!(this.x - ballImages[id].getWidth() >= paddlePos.x - paddleWidth / 2 && this.x <= paddlePos.x + paddleWidth / 2)) {
			return false;
		}
		return this.y + ballImages[id].getHeight() / 2 >= paddlePos.y - paddleHeight / 2 && this.y <= paddlePos.y;
	}

	public boolean isOutOfBounds() {
		return this.y > Main.getWindowHeight();
	}
}
