package minigames.brickbreaker;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * This is a visualisation for two power-ups that leave trails behind
 * this is the trail left behind
 * it needs to be updated
 */
public class Trail {
    private static Image[] trailExplosive;
    private static Image[] trailComet;

    public static void init() throws SlickException {
        trailExplosive = new Image[4];
        for (int i = 0; i < trailExplosive.length; i++) {
            trailExplosive[i] = new Image("img/brickbreaker/trails/trailExplosive" + (String.valueOf((i + 1)).length() == 1 ? "0" : "") + (i + 1) + ".png");
        }

        trailComet = new Image[4];
        for (int i = 0; i < trailComet.length; i++) {
            trailComet[i] = new Image("img/brickbreaker/trails/trailComet" + (String.valueOf((i + 1)).length() == 1 ? "0" : "") + (i + 1) + ".png");
        }
    }

    private HashMap<Ball, Point[]> prevPos = new HashMap<>();

    private boolean explosive = false;

    public void updateTrail(boolean explosive, ArrayList<Ball> balls) {
        if(this.explosive != explosive) {
            prevPos.clear();
        }
        this.explosive = explosive;
        Image[] trail = explosive ? trailExplosive : trailComet;
        for(int i = 0; i < balls.size(); i++) {
            if(balls.get(i).isOutOfBounds()) {
                prevPos.remove(balls.get(i));
            }
            if(!prevPos.containsKey(balls.get(i))) {
                Point[] points = new Point[trail.length];
                points[0] = new Point((int)balls.get(i).x, (int)balls.get(i).y);
                for(int a = 1; a < points.length; a++) {
                    points[a] = new Point(-100, -100);
                }
                prevPos.put(balls.get(i), points);
            } else {
                Point[] prev = prevPos.get(balls.get(i));
                for(int a = prev.length-1; a >= 0; a--) {
                    if(a - 1 >= 0) {
                        prev[a] = prev[a-1];
                    } else {
                        prev[a] = new Point((int)balls.get(i).x, (int)balls.get(i).y);
                    }
                }
            }
        }
    }

    public void drawTrail(boolean explosive) {
        Image[] trail = explosive ? trailExplosive : trailComet;
        for(Ball b : prevPos.keySet()) {
            Point[] prev = prevPos.get(b);
            for(int i = 0; i < prev.length; i++) {
                trail[i].draw(prev[i].x-trail[i].getWidth()/2, prev[i].y-trail[i].getHeight()/2);
            }
        }
    }
}
