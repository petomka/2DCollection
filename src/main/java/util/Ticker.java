package util;

/**
 * Created by Benedikt on 22.05.2017.
 */
public class Ticker {

    public interface Tickable {
        public void onTick();
    }

    private boolean interrupted;
    private int tickInterval = 1;
    private int ticks;
    private int delay = 0;

    private Tickable tickable;

    public Ticker(Tickable tickable) {
        this.tickable = tickable;
    }

    public void tick() {
        if(delay > 0) {
            delay -= 1;
            return;
        }
        if(ticks <= tickInterval) {
            ticks+=1;
            return;
        }
        try {
            tickable.onTick();
        } catch (Exception e) {
            e.printStackTrace();
        }
        ticks = 0;
    }

    public boolean isInterrupted() {
        return interrupted;
    }

    public void setInterrupted(boolean interrupted) {
        this.interrupted = interrupted;
    }

    public int getTickInterval() {
        return tickInterval;
    }

    public void setTickInterval(int tickInterval) {
        if(tickInterval < 1) {
            tickInterval = 1;
        }
        this.tickInterval = tickInterval;
    }

    public int getTicks() {
        return ticks;
    }

    public void setTicks(int ticks) {
        this.ticks = ticks;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }
}
