package util;

/**
 * Created by Benedikt on 22.05.2017.
 */
public class Point4 {
    public int x;
    public int y;
    public int z;
    public int a;

    public Point4() {
        this(0,0,0, 0);
    }

    public Point4(int x, int y) {
        this(x, y, 0, 0);
    }

    public Point4(int x, int y, int z) {
        this(x, y, z, 0);
    }

    public Point4(int x, int y, int z, int a) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.a = a;
    }


}
