package util;

/**
 * Created by Benedikt on 22.05.2017.
 */
public class Point3 {
    public int x;
    public int y;
    public int z;

    public Point3() {
        this(0,0,0);
    }

    public Point3(int x, int y) {
        this(x, y, 0);
    }

    public Point3(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }
}
