package util;

import java.io.*;
import java.lang.reflect.Array;

public class CopyUtils<T> {

	private Class<T> typeClass;

	public CopyUtils(Class<T> typeClass) {
		this.typeClass = typeClass;
	}

	public T[] deepArrayCopyOf(T[] array) {
		T[] copy = (T[]) Array.newInstance(typeClass, array.length);
		for (int index = 0; index < array.length; index++) {
			T t = array[index];
			if (t == null) {
				copy[index] = null;
			} else {
				copy[index] = copy(t);
			}
		}
		return copy;
	}

	public T[][] deep2DArrayCopyOf(T[][] array) {
		T[][] copy = (T[][]) Array.newInstance(typeClass, array.length, array[0].length);
		for (int index = 0; index < array.length; index++) {
			T[] innerArray = array[index];
			T[] copiedInner = deepArrayCopyOf(innerArray);
			copy[index] = copiedInner;
		}
		return copy;
	}

	public T copy(T orig) {
		Object obj = null;
		try {
			// Write the object out to a byte array
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream out = new ObjectOutputStream(bos);
			out.writeObject(orig);
			out.flush();
			out.close();

			// Make an input stream from the byte array and read
			// a copy of the object back in.
			ObjectInputStream in = new ObjectInputStream(
					new ByteArrayInputStream(bos.toByteArray()));
			obj = in.readObject();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return (T) obj;
	}

}
